import React, { useEffect, useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './UpdateBookPage.css';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import HomeIcon from '@mui/icons-material/Home';
import UpdateIcon from '@mui/icons-material/Update';
/**
 * Typ danych reprezentujący książkę.
 */
type Book = {
  title: string;
  author: string;
  publisher: string;
  publishYear: number | undefined;
  availableCopies: number | undefined;
  isbn: string;
};

type BookDataKey = keyof Book;
/**
 * Komponent funkcyjny do aktualizacji danych książki.
 */
export default function UpdateBookPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [bookId, setBookId] = useState('');
  const [bookData, setBookData] = useState<Book>({
    title: '',
    author: '',
    publisher: '',
    publishYear: undefined,
    availableCopies: undefined,
    isbn: '',
  });
  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    setIsFormValid(bookId.trim() !== '');
  }, [bookId]);
  /**
   * Obsługuje akcję aktualizacji książki.
   * Tworzy zaktualizowane dane z bookData i wysyła żądanie aktualizacji przez API.
   */
  const handleUpdateBook = async () => {
    try {
      const updatedData: Partial<Book> = {};
      (Object.keys(bookData) as BookDataKey[]).forEach((key) => {
        const value = bookData[key];
        if (value !== '' && value !== undefined) {
          if (key === 'publishYear' || key === 'availableCopies') {
            updatedData[key] = Number(value);
          } else {
            // @ts-ignore
            updatedData[key] = value;
          }
        }
      });

      console.log('Book ID:', bookId);
      console.log('Updated Data:', updatedData);

      const response = await apiClient.updateBook(
        parseInt(bookId),
        updatedData,
      );
      if (response.success) {
        toast.success(t('UpdateBookSucceeded'));
        navigate('/list-of-books');
      } else {
        toast.error(t('UpdateBookFailed'));
      }
    } catch (error) {
      toast.error(t('UpdateBookFailed'));
    }
  };
  /**
   * Obsługuje zmianę wartości w polu danych książki.
   * Aktualizuje odpowiednie pole w bookData.
   */
  const handleInputChange = (field: BookDataKey, value: string) => {
    setBookData((prevData) => ({
      ...prevData,
      [field]:
        field === 'publishYear' || field === 'availableCopies'
          ? value !== ''
            ? Number(value)
            : undefined
          : value,
    }));
  };

  return (
    <Box className="update-book-container">
      <img
        src={process.env.PUBLIC_URL + '/updateBook.png'}
        alt={t('updateBookAlt')}
        className="update-book-image"
      />
      <Typography variant="h2" className="update-book-title">
        {t('update_book')}
        <UpdateIcon style={{ marginRight: '4px' }} />
      </Typography>
      <TextField
        label={t('BookID')}
        value={bookId}
        onChange={(e) => setBookId(e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('Title')}
        value={bookData.title}
        onChange={(e) => handleInputChange('title', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('Author')}
        value={bookData.author}
        onChange={(e) => handleInputChange('author', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('Publisher')}
        value={bookData.publisher}
        onChange={(e) => handleInputChange('publisher', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('PublishYear')}
        type="number"
        value={bookData.publishYear !== undefined ? bookData.publishYear : ''}
        onChange={(e) => handleInputChange('publishYear', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('AvailableCopies')}
        type="number"
        value={
          bookData.availableCopies !== undefined ? bookData.availableCopies : ''
        }
        onChange={(e) => handleInputChange('availableCopies', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <TextField
        label={t('ISBN')}
        value={bookData.isbn}
        onChange={(e) => handleInputChange('isbn', e.target.value)}
        className="update-book-input"
        variant="outlined"
      />
      <Button
        variant="contained"
        color={isFormValid ? 'success' : 'inherit'}
        onClick={handleUpdateBook}
        className="update-book-button"
        disabled={!isFormValid}
      >
        {t('UpdateBook')}
      </Button>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
