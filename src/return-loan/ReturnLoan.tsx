import React, { useEffect, useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './ReturnLoan.css';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import HomeIcon from '@mui/icons-material/Home';
import SwapHorizontalCircleIcon from '@mui/icons-material/SwapHorizontalCircle';

/**
 * Komponent strony do zwracania wypożyczenia przez bibliotekarza
 * Umożliwia bibliotekarzowi zwrocenie książki oddanej przez czytelnika
 */
export default function ReturnLoanPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [loanId, setLoanId] = useState('');
  const [returnDate, setReturnDate] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    const today = new Date();
    const formattedDate = today.toISOString().split('T')[0];
    setReturnDate(formattedDate);
  }, []);

  useEffect(() => {
    setIsFormValid(loanId.trim() !== '' && returnDate.trim() !== '');
  }, [loanId, returnDate]);

  const handleReturnLoan = async () => {
    const today = new Date();
    const formattedToday = today.toISOString().split('T')[0];

    if (returnDate !== formattedToday) {
      toast.error(t('ReturnDateMustBeToday'));
      return;
    }

    try {
      const response = await apiClient.returnLoan(parseInt(loanId), returnDate);
      if (response.success) {
        toast.success(t('ReturnLoanSucceeded'));
        navigate('/list-of-loans');
      } else {
        toast.error(t('ReturnLoanFailed'));
      }
    } catch (error) {
      toast.error(t('ReturnLoanFailed'));
    }
  };

  return (
    <Box className="return-loan-container">
      <img
        src={process.env.PUBLIC_URL + '/returnLoan.png'}
        alt={t('returnLoanAlt')}
        className="return-loan-image"
      />
      <Typography variant="h2" className="return-loan-title">
        {t('return_loan')}
        <SwapHorizontalCircleIcon style={{ marginRight: '4px' }} />
      </Typography>
      <TextField
        label={t('LoanID')}
        value={loanId}
        onChange={(e) => setLoanId(e.target.value)}
        className="return-loan-input"
        variant="outlined"
      />
      <TextField
        label={t('ReturnDate')}
        type="date"
        value={returnDate}
        onChange={(e) => setReturnDate(e.target.value)}
        className="return-loan-input"
        InputLabelProps={{
          shrink: true,
        }}
      />
      <Button
        variant="contained"
        color={isFormValid ? 'success' : 'inherit'}
        onClick={handleReturnLoan}
        className="return-loan-button"
        disabled={!isFormValid}
      >
        {t('ReturnLoan')}
      </Button>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
