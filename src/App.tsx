import React from 'react';
import LoginForm from './login-form/LoginForm';
import MainComponent from './list-of-books/MainComponent';
import MainComponentLoans from './list-of-loans/MainComponentLoans';
import HomePage from './home-page/HomePage';
import { Navigate, Route, Routes, BrowserRouter } from 'react-router-dom';
import AddNewBookPage from './add-new-book/AddNewBook';
import AddNewLoanPage from './add-new-loan/AddNewLoan';
import MainComponentUser from './list-of-users/MainComponentUser';
import AddNewUserPage from './add-new-user/AddNewUser';
import { ToastContainer } from 'react-toastify';
import ApiProvider from './api/ApiProvider';
import { I18nextProvider } from 'react-i18next';
import i18n from './i18';
import DeleteBookPage from './delete-book/DeleteBook';
import DeleteUserPage from './delete-user/DeleteUser';
import ReturnLoanPage from './return-loan/ReturnLoan';
import AccountInfoPage from './account-info/AccountInfo';
import AccountSettingsPage from './account-settings/AccountSettings';
import MyLoans from './my-loans/MyLoans';
import UpdateBookPage from './edit-book/UpdateBookPage';
import UpdateUserPage from './edit-user/UpdateUserPage';
/**
 * Główny komponent aplikacji.
 * @returns {JSX.Element} Element JSX reprezentujący aplikację.
 */
function App() {
  return (
    <BrowserRouter>
      <I18nextProvider i18n={i18n}>
        <ApiProvider>
          <Routes>
            <Route path="/home" element={<HomePage />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/list-of-books" element={<MainComponent />} />
            <Route path="/list-of-loans" element={<MainComponentLoans />} />
            <Route path="/add-new-book" element={<AddNewBookPage />} />
            <Route path="/add-new-loan" element={<AddNewLoanPage />} />
            <Route path="/return-loan" element={<ReturnLoanPage />} />
            <Route path="/list-of-users" element={<MainComponentUser />} />
            <Route path="/my-loans" element={<MyLoans />} />
            <Route path="/add-new-user" element={<AddNewUserPage />} />
            <Route path="/delete-book" element={<DeleteBookPage />} />
            <Route path="/edit-book" element={<UpdateBookPage />} />
            <Route path="/edit-user" element={<UpdateUserPage />} />
            <Route path="/delete-user" element={<DeleteUserPage />} />
            <Route path="/account-info" element={<AccountInfoPage />} />
            <Route path="/account-settings" element={<AccountSettingsPage />} />
            <Route path="/" element={<Navigate to="/login" />} />
            <Route path="*" element={<h1>404 FAILED</h1>} />
          </Routes>
          <ToastContainer />
        </ApiProvider>
      </I18nextProvider>
    </BrowserRouter>
  );
}

export default App;
