import React, { useEffect, useState } from 'react';
import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import HomeIcon from '@mui/icons-material/Home';
import { User } from '../list-of-users/IndividualUserFromList';
import './AccountSettings.css';
import SettingsIcon from '@mui/icons-material/Settings';
/**
 * Komponent strony do zaktualizowania danych użytkownika
 * Umożliwia użytkownikom aktualizację danych (nazwy, maila, pełnej nazwy użytkownika i hasła)
 */
export default function AccountSettingsPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [userData, setUserData] = useState<Partial<User>>({
    userName: '',
    email: '',
    fullUserName: '',
    password: '',
  });
  const [isFormValid, setIsFormValid] = useState(false);
  const [backendError, setBackendError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await apiClient.getCurrentUser();
        if (response.success && response.data) {
          const { userName, email, fullUserName } = response.data;
          setUserData({ userName, email, fullUserName, password: '' });
        } else {
          toast.error(t('FailedToFetchUserData'));
        }
      } catch (error) {
        toast.error(t('FailedToFetchUserData'));
      }
    };
    fetchData();
  }, [apiClient, t]);

  useEffect(() => {
    const { userName, email, fullUserName, password } = userData;
    setIsFormValid(
      userName?.trim() !== '' &&
        email?.trim() !== '' &&
        fullUserName?.trim() !== '' &&
        password?.trim() !== '',
    );
  }, [userData]);

  useEffect(() => {
    if (backendError) {
      toast.error(backendError);
      setBackendError(null);
    }
  }, [backendError]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setUserData((prevUserData) => ({
      ...prevUserData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const response = await apiClient.updateUser(userData as User);
      if (response.success) {
        toast.success(t('UserDataUpdated'));
        navigate('/home');
      } else {
        if (response.statusCode === 400) {
          toast.error(t('InvalidDataFormat'));
        } else if (response.statusCode === 409) {
          toast.error(t('UserWithUsernameExists'));
        } else {
          toast.error(t('FailedToUpdateUserData'));
        }
      }
    } catch (error: any) {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        setBackendError(error.response.data.message);
      } else {
        toast.error(t('FailedToUpdateUserData'));
      }
    }
  };

  return (
    <Box className="account-settings-container">
      <img
        src={process.env.PUBLIC_URL + '/accountSettings.png'}
        alt={t('accountSettings')}
        className="account-settings-image"
      />
      <Typography variant="h1" className="account-settings-title">
        {t('AccountSettings')}
        <SettingsIcon style={{ fontSize: '2.5rem', marginRight: '4px' }} />
      </Typography>

      <form onSubmit={handleSubmit} className="account-settings-form">
        <TextField
          label={t('Username')}
          name="userName"
          value={userData.userName || ''}
          onChange={handleChange}
          className="account-settings-input"
          variant="outlined"
        />
        <TextField
          label={t('Email')}
          name="email"
          value={userData.email || ''}
          onChange={handleChange}
          className="account-settings-input"
          variant="outlined"
        />
        <TextField
          label={t('FullUsername')}
          name="fullUserName"
          value={userData.fullUserName || ''}
          onChange={handleChange}
          className="account-settings-input"
          variant="outlined"
        />

        <TextField
          label={t('Password')}
          name="password"
          type="password"
          value={userData.password || ''}
          onChange={handleChange}
          className="account-settings-input"
          variant="outlined"
        />
        <Button
          type="submit"
          variant="contained"
          color={isFormValid ? 'success' : 'inherit'}
          className="account-settings-button"
          disabled={!isFormValid}
        >
          {t('UpdateUserData')}
        </Button>
      </form>
      <Tooltip title={t('BackToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
