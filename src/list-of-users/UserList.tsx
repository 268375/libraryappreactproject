import React, { useState } from 'react';
import { Grid, TextField, Button } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import { Formik, Form, Field, FieldProps } from 'formik';
import './UserList.css';
import IndividualUserFromList from './IndividualUserFromList';
import { useTranslation } from 'react-i18next';
/**
 * Interfejs reprezentujący użytkownika.
 */
interface User {
  id: number;
  userName: string;
  password: string;
  role: string;
  email: string;
  fullUserName: string;
}
/**
 * Właściwości komponentu UserList.
 */
interface UserListProps {
  users: User[];
}
/**
 * Komponent wyświetlający listę użytkowników oraz umożliwiający filtrowanie i paginację.
 * @param {UserListProps} props - Właściwości komponentu.
 */
function UserList({ users }: UserListProps) {
  const { t } = useTranslation();

  const [searchTerm, setSearchTerm] = useState<string>('');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [usersPerPage, setUsersPerPage] = useState<number>(8);

  const filteredUsers = users.filter((user: User) => {
    return (
      user.userName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.email.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.fullUserName.toLowerCase().includes(searchTerm.toLowerCase())
    );
  });

  const indexOfFirstUserOnPage = (currentPage - 1) * usersPerPage;
  const indexOfLastUserOnPage = currentPage * usersPerPage;
  const currentUsersOnPage = filteredUsers.slice(
    indexOfFirstUserOnPage,
    indexOfLastUserOnPage,
  );

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  const nextPage = () => {
    if (currentPage < Math.ceil(filteredUsers.length / usersPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  /**
   * Obsługuje zmianę liczby użytkowników na stronę.
   * @param {React.ChangeEvent<HTMLSelectElement>} event - Zdarzenie zmiany wartości w polu wyboru.
   */
  const handleUsersPerPageChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
  ) => {
    const newUsersPerPage = parseInt(event.target.value);
    setUsersPerPage(newUsersPerPage);
    setCurrentPage(1);
  };

  return (
    <Formik
      initialValues={{ searchTerm: '' }}
      onSubmit={(values, actions) => {
        setSearchTerm(values.searchTerm);
        setCurrentPage(1);
        actions.setSubmitting(false);
      }}
    >
      <Form>
        <div className="searchFieldContainer">
          {' '}
          <Field name="searchTerm">
            {(props: FieldProps<string>) => (
              <TextField
                {...props.field}
                label={t('searchPlaceholder')}
                variant="outlined"
                InputProps={{
                  startAdornment: <SearchIcon className="iconStyle" />,
                }}
                className="searchField"
              />
            )}
          </Field>
        </div>
        <div className="usersGridContainer">
          {' '}
          <Grid container spacing={2} className="formField formContainer">
            {' '}
            {currentUsersOnPage.map((user: User) => (
              <Grid item key={user.id} xs={12} sm={6} md={4} lg={3}>
                <IndividualUserFromList user={user} />{' '}
              </Grid>
            ))}
          </Grid>
        </div>
        <Grid container className="formContainer" justifyContent="center">
          {' '}
          <Button onClick={prevPage} disabled={currentPage === 1}>
            {t('previous')}
          </Button>
          {Array.from({
            length: Math.ceil(filteredUsers.length / usersPerPage),
          }).map((_, index) => (
            <Button
              key={index}
              onClick={() => paginate(index + 1)}
              className="searchButton"
              disabled={currentPage === index + 1}
            >
              {index + 1}
            </Button>
          ))}
          <Button
            onClick={nextPage}
            disabled={
              currentPage === Math.ceil(filteredUsers.length / usersPerPage)
            }
          >
            {t('next')}
          </Button>
        </Grid>
        <div className="usersPerPageContainer">
          <div className="usersPerPageFrame">
            <AutoStoriesIcon className="usersPerPageIcon" />
            <label htmlFor="usersPerPageSelect" className="usersPerPageLabel">
              {t('usersPerPage')}
            </label>
            <select
              id="usersPerPageSelect"
              value={usersPerPage}
              onChange={handleUsersPerPageChange}
              className="usersPerPageSelect"
            >
              <option value={4}>4</option>
              <option value={8}>8</option>
              <option value={12}>12</option>
            </select>
          </div>
        </div>
      </Form>
    </Formik>
  );
}

export default UserList;
