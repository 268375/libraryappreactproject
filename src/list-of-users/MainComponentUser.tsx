import React, { useState, useEffect } from 'react';
import './MainComponentUser.css';
import { useNavigate } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import { Box, Tooltip } from '@mui/material';
import UserList from '../list-of-users/UserList';
import { User } from '../list-of-users/IndividualUserFromList';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent odpowiedzialny za wyświetlanie listy użytkowników.
 */
function MainComponentUser() {
  const { t } = useTranslation();
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const navigate = useNavigate();
  const apiClient = useApi();

  useEffect(() => {
    /**
     * Pobiera listę użytkowników z API i aktualizuje stan komponentu.
     */
    const fetchUsers = async () => {
      const response = await apiClient.getUsers();
      if (response.success) {
        setUsers(response.data);
      } else {
        setError(t('errorFetchingUsers'));
      }
      setLoading(false);
    };

    fetchUsers();
  }, []);

  if (loading) {
    return <div>{t('loading')}</div>;
  }

  if (error) {
    return (
      <div>
        {t('errorFetchingUsers')}: {error}
      </div>
    );
  }

  return (
    <Box className="main-container">
      <img
        src={process.env.PUBLIC_URL + '/users.png'}
        alt={t('usersAlt')}
        className="users-image"
      />
      <h1> {t('userListTitle')}</h1>
      <UserList users={users} />
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}

export default MainComponentUser;
