import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';
import './IndividualUserFromList.css';
import { useTranslation } from 'react-i18next';
/**
 * Interfejs reprezentujący użytkownika.
 */
export interface User {
  id: number;
  userName: string;
  password: string;
  role: string;
  email: string;
  fullUserName: string;
}
/**
 * Komponent reprezentujący pojedynczego użytkownika na liście.
 * @param {Object} props - Właściwości komponentu.
 * @param {User} props.user - Użytkownik do wyświetlenia.
 */
function IndividualUserFromList({ user }: { user: User }) {
  const { t } = useTranslation();

  return (
    <Card className="user-item">
      {' '}
      {}
      <CardContent>
        <Typography color="textSecondary">
          {' '}
          {t('userID')}: {user.id}
        </Typography>{' '}
        <Typography color="textSecondary" className="bold">
          <strong>
            {' '}
            {t('username')}: {user.userName}
          </strong>
        </Typography>
        <Typography color="textSecondary" className="italic">
          {' '}
          {t('fullUsername')}: {user.fullUserName}
        </Typography>
        <Typography color="textSecondary">
          {' '}
          {t('email')}: {user.email}
        </Typography>
        <Typography color="textSecondary">
          {' '}
          {t('role')}: {user.role}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default IndividualUserFromList;
