import './LoginForm.css';
import {
  Button,
  TextField,
  Box,
  Typography,
  Menu,
  MenuItem,
} from '@mui/material';
import LoginIcon from '@mui/icons-material/Login';
import { Formik } from 'formik';
import React, { useCallback, useMemo, useState } from 'react';
import * as yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import EmailIcon from '@mui/icons-material/Email';
import PhoneIcon from '@mui/icons-material/Phone';
import LocationOnIcon from '@mui/icons-material/LocationOn';
/**
 * Komponent reprezentujący formularz logowania do aplikacji biblioteki
 */
function LoginForm() {
  const navigate = useNavigate();

  const { t, i18n } = useTranslation();
  const apiClient = useApi();
  const [languageAnchorEl, setLanguageAnchorEl] = useState<null | HTMLElement>(
    null,
  );
  /**
   * Obsługuje otwarcie menu wyboru języka.
   * @param {React.MouseEvent<HTMLButtonElement>} event - Zdarzenie kliknięcia przycisku.
   */
  const handleLanguageMenuOpen = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    setLanguageAnchorEl(event.currentTarget);
  };
  /**
   * Obsługuje zamknięcie menu wyboru języka.
   */
  const handleLanguageMenuClose = () => {
    setLanguageAnchorEl(null);
  };
  /**
   * Obsługuje kliknięcie na opcję wyboru języka.
   * @param {string} language - Wybrany język.
   */
  const handleLanguageItemClick = (language: string) => {
    i18n.changeLanguage(language);
    handleLanguageMenuClose();
  };

  const buttonStyles = {
    greenButton: {
      backgroundColor: '#4caf50',
      color: '#fff',
      '&:hover': {
        backgroundColor: '#388e3c',
      },
    },
    greyButton: {
      backgroundColor: 'lightgrey',
      color: '#fff',
    },
    lightGreyButton: {
      backgroundColor: 'grey',
      color: '#fff',
    },
  };
  /**
   * Obsługuje logowanie użytkownika.
   * @param {Object} values - Wartości formularza (login i hasło).
   * @param {any} formik - Obiekt Formik.
   */
  const onSubmit = useCallback(
    (values: { login: string; password: string }, formik: any) => {
      apiClient.login(values).then((response) => {
        if (response.success) {
          //zapisanie tokena w localStorage po udanym logowaniu
          localStorage.setItem('token', response.data?.token as string);
          navigate('/home');
        } else {
          formik.setFieldError('login', t('invalidLoginOrPassword'));
          formik.setFieldError('password', t('invalidLoginOrPassword'));
        }
      });
    },
    [t, apiClient, navigate],
  );

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        login: yup.string().required(t('required')),
        password: yup
          .string()
          .required(t('required'))
          .min(5, t('passwordTooShort')),
      }),
    [t],
  );

  return (
    <Formik
      initialValues={{ login: '', password: '' }}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      validateOnChange
      validateOnBlur
    >
      {(formik: any) => (
        <div>
          <img
            src={process.env.PUBLIC_URL + '/data-protection.png'}
            alt="Log"
            style={{
              position: 'absolute',
              top: 300,
              left: 300,
              width: '400px',
              height: '400px',
            }}
          />
          <form
            className="Login-form"
            id="signForm"
            onSubmit={formik.handleSubmit}
            noValidate
          >
            <Typography
              variant="h2"
              color="textSecondary"
              className="bold-login"
            >
              {t('logInHello')}
            </Typography>
            <TextField
              id="login"
              label={t('loginLabel')}
              variant="standard"
              name="login"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.login && !!formik.errors.login}
              helperText={formik.touched.login && formik.errors.login}
            />
            <TextField
              id="password"
              label={t('passwordLabel')}
              variant="standard"
              type="password"
              name="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.password && formik.errors.password}
            />
            <Button
              variant="contained"
              startIcon={<LoginIcon />}
              type="submit"
              form="signForm"
              disabled={!(formik.isValid && formik.dirty)}
              style={
                formik.isValid && formik.dirty
                  ? buttonStyles.greenButton
                  : buttonStyles.greyButton
              }
            >
              {t('signIn')}
            </Button>
            <Button
              aria-controls="language-menu-appbar"
              aria-haspopup="true"
              onClick={handleLanguageMenuOpen}
              style={buttonStyles.lightGreyButton}
            >
              {t('languageChoice')}
            </Button>
            <Menu
              id="language-menu-appbar"
              anchorEl={languageAnchorEl}
              open={Boolean(languageAnchorEl)}
              onClose={handleLanguageMenuClose}
            >
              <MenuItem onClick={() => handleLanguageItemClick('en')}>
                English
              </MenuItem>
              <MenuItem onClick={() => handleLanguageItemClick('pl')}>
                Polski
              </MenuItem>
            </Menu>
          </form>
          <Box className="contact-bar-login">
            <Box className="contact-item">
              <LocationOnIcon />
              <Typography variant="body1" color="textSecondary">
                {t('adress')}
              </Typography>
            </Box>
            <Box className="contact-item">
              <PhoneIcon />
              <Typography variant="body1" color="textSecondary">
                123-456-789
              </Typography>
            </Box>
            <Box className="contact-item">
              <EmailIcon />
              <Typography variant="body1" color="textSecondary">
                kontakt@biblioteka.pl
              </Typography>
            </Box>
          </Box>
        </div>
      )}
    </Formik>
  );
}

export default LoginForm;
