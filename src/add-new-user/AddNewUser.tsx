import React, { useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {
  Button,
  TextField,
  Typography,
  Paper,
  Box,
  Tooltip,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './AddNewUser.css';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent AddNewUserPage.
 * Strona dodawania nowego użytkownika.
 * @component
 */
export default function AddNewUserPage() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const apiClient = useApi();
  const [formData, setFormData] = useState({
    userName: '',
    password: '',
    role: '',
    email: '',
    fullUserName: '',
    errors: {
      userName: '',
      password: '',
      role: '',
      email: '',
      fullUserName: '',
    },
  });
  /**
   * Sprawdza, czy formularz jest prawidłowo wypełniony.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */
  const isFormValid = () => {
    return (
      formData.userName.trim() !== '' &&
      formData.password.trim() !== '' &&
      formData.role.trim() !== '' &&
      formData.email.trim() !== '' &&
      formData.fullUserName.trim() !== ''
    );
  };
  /**
   * Obsługuje zmianę wartości pól formularza.
   * @param {object} e - Obiekt zdarzenia.
   */
  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  /**
   * Obsługuje przesłanie formularza.
   * @param {object} e - Obiekt zdarzenia.
   */
  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    if (validateForm()) {
      try {
        const response = await apiClient.addUser(formData); //wysłanie żądanie dodania użytkownika do serwera
        if (response.success) {
          console.log('User added successfully:', response.data);
          toast.success(t('userAddedSuccessfully'));
          setFormData({
            userName: '',
            password: '',
            role: '',
            email: '',
            fullUserName: '',
            errors: {
              userName: '',
              password: '',
              role: '',
              email: '',
              fullUserName: '',
            },
          });
          navigate('/list-of-users');
        } else {
          switch (response.statusCode) {
            case 409:
              toast.error(t('userAlreadyExists'));
              break;
            case 400:
              toast.error(t('incompleteOrInvalidUserInfo'));
              break;
            default:
              toast.error(t('errorAddingUser'));
          }
        }
      } catch (error) {
        console.error('Error adding user:', error);
        toast.error(t('errorAddingUser'));
      }
    }
  };
  /**
   * Waliduje formularz.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */
  const validateForm = () => {
    const errors = {
      userName: '',
      password: '',
      role: '',
      email: '',
      fullUserName: '',
    };
    let isValid = true;

    if (formData.userName.length < 3 || formData.userName.length > 16) {
      errors.userName = t('userNameLengthError');
      isValid = false;
    }

    if (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(formData.password)) {
      errors.password = t('passwordCriteriaError');
      isValid = false;
    }

    if (formData.role !== 'ROLE_READER' && formData.role !== 'ROLE_LIBRARIAN') {
      errors.role = t('roleError');
      isValid = false;
    }

    if (!/\S+@\S+\.\S+/.test(formData.email)) {
      errors.email = t('emailError');
      isValid = false;
    }

    if (formData.fullUserName.length < 3 || formData.fullUserName.length > 50) {
      errors.fullUserName = t('fullUserNameLengthError');
      isValid = false;
      isValid = false;
    }

    setFormData({
      ...formData,
      errors: errors,
    });

    return isValid;
  };

  return (
    <Paper className="container">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        style={{
          color: '#555',
          fontWeight: 'bold',
          textAlign: 'center',
          padding: '10px 0',
          borderRadius: '4px',
        }}
      >
        <Typography variant="h4" component="span">
          {t('addNewUserTitle')}
        </Typography>
        <AddCircleOutlineIcon style={{ marginRight: '8px' }} />
      </Box>
      <img
        src={process.env.PUBLIC_URL + '/addUser.png'}
        alt={t('addUserAltText')}
        style={{
          position: 'absolute',
          top: 150,
          left: 700,
          width: '270px',
          height: '270px',
        }}
      />
      <form className="form" onSubmit={handleSubmit}>
        <TextField
          className="form-field"
          name="userName"
          label={t('userNameLabel')}
          value={formData.userName}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.userName ? true : false}
          helperText={formData.errors.userName}
        />
        <TextField
          className="form-field"
          name="password"
          label={t('passwordLabel')}
          type="password"
          value={formData.password}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.password ? true : false}
          helperText={formData.errors.password}
        />
        <TextField
          className="form-field"
          name="role"
          label={t('roleLabel')}
          value={formData.role}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.role ? true : false}
          helperText={formData.errors.role}
        />
        <TextField
          className="form-field"
          name="email"
          label="Email"
          type="email"
          value={formData.email}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.email ? true : false}
          helperText={formData.errors.email}
        />
        <TextField
          className="form-field"
          name="fullUserName"
          label={t('fullUserNameLabel')}
          value={formData.fullUserName}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.fullUserName ? true : false}
          helperText={formData.errors.fullUserName}
        />
        <Button
          className="submit-button"
          variant="contained"
          type="submit"
          style={{
            backgroundColor: isFormValid() ? '#555555' : '#ff6666',
            color: '#fff',
            fontWeight: 'bold',
            textAlign: 'center',
            width: '100%',
            display: 'block',
          }}
          disabled={!isFormValid()} //przycisk dodania użytkownika jest zablokowany (czerwony), póki każde z pól nie jest poprawne
        >
          {t('addUserButton')}
        </Button>
        <Tooltip title={t('backToHome')}>
          <HomeIcon
            className="back-to-home-icon"
            onClick={() => navigate('/home')}
          />
        </Tooltip>
      </form>
    </Paper>
  );
}
