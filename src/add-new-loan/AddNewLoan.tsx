import React, { useState } from 'react';
import {
  Button,
  TextField,
  Typography,
  Paper,
  Box,
  Tooltip,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './AddNewLoan.css';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
import { toast } from 'react-toastify';
import { NewloanDto } from '../api/dto/newloan.dto';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent AddNewLoanPage.
 * Strona dodawania nowego wypożyczenia książki.
 * @component
 */
export default function AddNewLoanPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [formData, setFormData] = useState({
    userId: '',
    bookId: '',
    loanDate: new Date().toISOString().split('T')[0], // loan date domyślnie ustawiana na datę dzisiejszą
    deadlineDate: '',
    errors: {
      userId: '',
      bookId: '',
      loanDate: '',
      deadlineDate: '',
    },
  });
  /**
   * Sprawdza, czy formularz jest prawidłowo wypełniony.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */
  const isFormValid = () => {
    return (
      formData.userId.trim() !== '' &&
      formData.bookId.trim() !== '' &&
      formData.loanDate.trim() !== '' &&
      formData.deadlineDate.trim() !== '' &&
      new Date(formData.deadlineDate) > new Date(formData.loanDate)
    );
  };
  /**
   * Obsługuje zmianę wartości pól formularza.
   * @param {object} e - Obiekt zdarzenia.
   */
  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  /**
   * Obsługuje przesłanie formularza.
   * @param {object} e - Obiekt zdarzenia.
   */ const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    if (validateForm()) {
      const newLoan: NewloanDto = {
        user: { id: formData.userId },
        book: { id: formData.bookId },
        loanDate: formData.loanDate,
        deadlineDate: formData.deadlineDate,
        returnDate: null,
      };

      try {
        const response = await apiClient.addLoan(newLoan); // wysłanie żądania dodania wypożyczenia do serwera
        if (response.success) {
          console.log('Loan added successfully:', response.data);
          toast.success(t('loanAddedSuccessfully'));
          setFormData({
            userId: '',
            bookId: '',
            loanDate: new Date().toISOString().split('T')[0], // loan date domyślnie ustawiana na datę dzisiejszą
            deadlineDate: '',
            errors: {
              userId: '',
              bookId: '',
              loanDate: '',
              deadlineDate: '',
            },
          });
          navigate('/list-of-loans');
        } else {
          switch (response.statusCode) {
            case 400:
              toast.error(t('incompleteOrInvalidLoanInfo'));
              break;
            case 404:
              toast.error(t('userOrBookNotFound'));
              break;
            default:
              toast.error(t('errorAddingLoan'));
          }
        }
      } catch (error) {
        console.error('Error adding loan:', error);
        toast.error(t('errorAddingLoan'));
      }
    }
  };
  /**
   * Waliduje formularz.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */
  const validateForm = () => {
    const errors = {
      userId: '',
      bookId: '',
      loanDate: '',
      deadlineDate: '',
      returnDate: '',
    };
    let isValid = true;

    if (formData.deadlineDate < formData.loanDate) {
      errors.deadlineDate = t('deadlineDateError');

      isValid = false;
    }
    setFormData({
      ...formData,
      errors: errors,
    });

    return isValid;
  };

  return (
    <Paper className="container">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        style={{
          color: '#555',
          fontWeight: 'bold',
          textAlign: 'center',
          padding: '10px 0',
          borderRadius: '4px',
        }}
      >
        <Typography variant="h4" component="span">
          {t('addNewLoanTitle')}
        </Typography>
        <AddCircleOutlineIcon style={{ marginRight: '8px' }} />
      </Box>
      <img
        src={process.env.PUBLIC_URL + '/borrow.png'}
        alt={t('addLoanAltText')}
        style={{
          position: 'absolute',
          top: 100,
          left: 700,
          width: '270px',
          height: '270px',
        }}
      />
      <form className="form" onSubmit={handleSubmit}>
        <TextField
          className="form-field"
          name="userId"
          label={t('userIDLabel')}
          value={formData.userId}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
        />
        <TextField
          className="form-field"
          name="bookId"
          label={t('bookIDLabel')}
          value={formData.bookId}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
        />
        <TextField
          className="form-field"
          name="loanDate"
          label={t('loanDateLabel')}
          type="date"
          value={formData.loanDate}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
            shrink: true,
          }}
        />
        <TextField
          className="form-field"
          name="deadlineDate"
          label={t('deadlineDateLabel')}
          type="date"
          value={formData.deadlineDate}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
            shrink: true,
          }}
          error={formData.errors.deadlineDate ? true : false}
          helperText={formData.errors.deadlineDate}
        />
        <Button
          className="submit-button"
          variant="contained"
          type="submit"
          style={{
            backgroundColor: isFormValid() ? '#555555' : '#ff6666',
            color: '#fff',
            fontWeight: 'bold',
            textAlign: 'center',
            width: '100%',
            display: 'block',
            marginTop: '20px',
          }}
          disabled={!isFormValid()} // przycisk dodania wypożyczenia jest zablokowany, póki każde z pól nie jest poprawne
        >
          {t('addLoanButton')}
        </Button>
      </form>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Paper>
  );
}
