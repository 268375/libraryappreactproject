import { Box, Typography } from '@mui/material';
import MenuAppBar from '../menu-app-bar/MenuAppBar';
import { Outlet } from 'react-router-dom';
import EmailIcon from '@mui/icons-material/Email';
import PhoneIcon from '@mui/icons-material/Phone';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import React from 'react';
import './HomePage.css';
import { useTranslation } from 'react-i18next';
/**
 * Komponent HomePage.
 *
 * @component
 * @returns {JSX.Element} JSX kod renderujący stronę główną.
 */
function HomePage() {
  const { t } = useTranslation();
  return (
    <Box
      sx={{
        flexGrow: 1,
        backgroundImage: 'url(/library-bg.jpg)',
        backgroundSize: 'cover',
        minHeight: '100vh',
      }}
    >
      <MenuAppBar />
      <Box
        sx={{
          mt: 4,
          textAlign: 'center',
          backgroundColor: 'rgba(255, 255, 255, 0.8)',
          padding: '20px',
          borderRadius: '10px',
          margin: '20px',
        }}
      >
        <Typography variant="h2" color="textSecondary" className="bold">
          {t('welcome')}
        </Typography>
        <img src="/open.jpg" alt={'Library'} className="open-book-item" />
        <Typography color="textSecondary" className="italic">
          {t('cite')}
        </Typography>
        <Outlet />
      </Box>
      <Box className="contact-bar-home">
        <Box className="contact-item">
          <LocationOnIcon />
          <Typography variant="body1" color="textSecondary">
            {t('adress')}
          </Typography>
        </Box>
        <Box className="contact-item">
          <PhoneIcon />
          <Typography variant="body1" color="textSecondary">
            123-456-789
          </Typography>
        </Box>
        <Box className="contact-item">
          <EmailIcon />
          <Typography variant="body1" color="textSecondary">
            kontakt@biblioteka.pl
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default HomePage;
