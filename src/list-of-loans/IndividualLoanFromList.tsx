import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';
import { Book } from '../list-of-books/IndividualBookFromList';
import { User } from '../list-of-users/IndividualUserFromList'; //import interfejsu Book
import './IndividualLoanFromList.css';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
/**
 * Interfejs reprezentujący wypożyczenie.
 */
export interface Loan {
  id: number;
  user: User;
  book: Book;
  loanDate: string;
  deadlineDate: string;
  returnDate: string | null;
}

/**
 * Komponent reprezentujący pojedyncze wypożyczenie na liście.
 * @param {Object} props - Właściwości komponentu.
 * @param {Loan} props.loan - Wypożyczenie do wyświetlenia.
 */
function IndividualLoanFromList({ loan }: { loan: Loan }) {
  const { t } = useTranslation();
  const cardClass = classNames('loan-item', {
    'not-returned': !loan.returnDate,
  });

  return (
    <Card className={cardClass}>
      <CardContent>
        <Typography color="textSecondary" className="bold">
          {' '}
          <strong>
            {t('loanID')}: {loan.id}
          </strong>
        </Typography>
        <Typography color="textSecondary">
          {t('userID')}: {loan.user.id}
        </Typography>
        <Typography color="textSecondary">
          {t('bookID')}: {loan.book.id}
        </Typography>
        <Typography color="textSecondary">
          {t('loanDate')}: {''} {new Date(loan.loanDate).toLocaleDateString()}
        </Typography>
        <Typography color="textSecondary">
          {t('deadlineDate')}:{' '}
          {new Date(loan.deadlineDate).toLocaleDateString()}
        </Typography>
        <Typography color="textSecondary">
          {t('returnDate')}:{' '}
          {loan.returnDate
            ? new Date(loan.returnDate).toLocaleDateString()
            : t('notReturned')}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default IndividualLoanFromList;
