import React, { useEffect, useState } from 'react';
import './MainComponentLoans.css';
import { useNavigate } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import { Box, Tooltip } from '@mui/material';
import LoanList from './ListOfLoans';
import { Loan } from '../list-of-loans/IndividualLoanFromList';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent odpowiedzialny za wyświetlanie listy wypożyczeń.
 */
function MainComponentLoans() {
  const { t } = useTranslation();
  const [loans, setLoans] = useState<Loan[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const navigate = useNavigate();
  const apiClient = useApi();
  useEffect(() => {
    /**
     * Pobiera listę wypożyczeń z API i aktualizuje stan komponentu.
     */
    const fetchLoans = async () => {
      const response = await apiClient.getLoans();
      if (response.success) {
        setLoans(response.data);
      } else {
        setError(t('errorFetchingLoans'));
      }
      setLoading(false);
    };

    fetchLoans();
  }, []);

  if (loading) {
    return <div>{t('loading')}</div>;
  }

  if (error) {
    return (
      <div>
        {t('errorFetchingLoans')}: {error}
      </div>
    );
  }
  return (
    <div className="main-container">
      <img
        src={process.env.PUBLIC_URL + '/loan.jpg'}
        alt={t('loans')}
        className="loan-image"
      />
      <h1>{t('listOfLoans')}</h1>
      <LoanList loans={loans} />
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </div>
  );
}

export default MainComponentLoans;
