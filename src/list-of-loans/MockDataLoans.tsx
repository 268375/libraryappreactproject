import { mockBooks } from '../list-of-books/MockData'; // import mockBooks z przykladowaymi danymi książek

// przykładowe dane użytkowników
const mockUsers = [
  {
    id: 1,
    userName: 'NikodemW',
    password: '$2a$10$wDcITG8DTVtCUppqeEhOYOmRJaU2Y7tyqRzWH72cAq4DIts7N4m0.',
    role: 'ROLE_READER',
    email: 'nikiwewe@gmail.com',
    fullUserName: 'Nikodem Wers',
  },
  {
    id: 2,
    userName: 'AnkaK',
    password: '$2a$10$wDcITG8DTVtCUppqeEhOYOmRJaU2Y7wqdEARSX42cAq4DIts7N4m0.',
    role: 'ROLE_READER',
    email: 'anka@gmail.com',
    fullUserName: 'Anna Kot',
  },
  {
    id: 3,
    userName: 'ZuzannaWe',
    password: '$2a$10$wDcITG8DtywrCUppqeEhOYOmRJaU2Y7wqdEARSX42cAq4DIts7N4m0.',
    role: 'ROLE_READER',
    email: 'zuwe@gmail.com',
    fullUserName: 'Zuzanna Werona',
  },
];

export const mockLoans = [
  {
    id: 1,
    user: mockUsers[0],
    book: mockBooks[0],
    loanDate: '2024-03-29',
    deadlineDate: '2024-04-10',
    returnDate: null,
  },
  {
    id: 2,
    user: mockUsers[0],
    book: mockBooks[1],
    loanDate: '2024-03-30',
    deadlineDate: '2024-04-11',
    returnDate: null,
  },
  {
    id: 3,
    user: mockUsers[0],
    book: mockBooks[2],
    loanDate: '2024-04-11',
    deadlineDate: '2024-04-15',
    returnDate: '2024-04-12',
  },
  {
    id: 4,
    user: mockUsers[1],
    book: mockBooks[4],
    loanDate: '2024-04-21',
    deadlineDate: '2024-04-30',
    returnDate: '2024-04-28',
  },
  {
    id: 5,
    user: mockUsers[2],
    book: mockBooks[6],
    loanDate: '2024-05-10',
    deadlineDate: '2024-05-20',
    returnDate: '2024-05-16',
  },
  {
    id: 6,
    user: mockUsers[2],
    book: mockBooks[7],
    loanDate: '2024-05-11',
    deadlineDate: '2024-05-21',
    returnDate: '2024-05-17',
  },
  {
    id: 7,
    user: mockUsers[1],
    book: mockBooks[9],
    loanDate: '2024-05-15',
    deadlineDate: '2024-05-27',
    returnDate: null,
  },
  {
    id: 8,
    user: mockUsers[2],
    book: mockBooks[10],
    loanDate: '2024-05-16',
    deadlineDate: '2024-05-28',
    returnDate: null,
  },
  {
    id: 9,
    user: mockUsers[2],
    book: mockBooks[3],
    loanDate: '2024-05-17',
    deadlineDate: '2024-05-28',
    returnDate: null,
  },
];
