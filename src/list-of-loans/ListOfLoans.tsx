import React, { useEffect, useState } from 'react';
import { Grid, TextField, Button } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import { Formik, Form, Field, FieldProps } from 'formik';
import './ListOfLoans.css';
import IndividualLoanFromList from './IndividualLoanFromList';
import { useTranslation } from 'react-i18next';
import { User } from '../list-of-users/IndividualUserFromList';
import { Book } from '../list-of-books/IndividualBookFromList';
/**
 * Interfejs reprezentujący wypożyczenie.
 */
interface Loan {
  id: number;
  user: User;
  book: Book;
  loanDate: string;
  deadlineDate: string;
  returnDate: string | null;
}

/**
 * Właściwości komponentu ListOfLoans.
 */
interface LoanListProps {
  loans: Loan[];
}
/**
 * Komponent ListOfLoans wyświetlający listę wypożyczeń.
 * @param {LoanListProps} props - Właściwości komponentu.
 */
function ListOfLoans({ loans }: LoanListProps) {
  const { t } = useTranslation();

  const [searchTerm, setSearchTerm] = useState<string>('');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [loansPerPage, setLoansPerPage] = useState<number>(8);

  const filteredLoans = loans.filter((loan: Loan) => {
    const loanDate = new Date(loan.loanDate).toLocaleDateString();
    const deadlineDate = new Date(loan.deadlineDate).toLocaleDateString();

    return (
      loan.user.fullUserName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loan.book.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loan.book.author.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loanDate.includes(searchTerm) ||
      deadlineDate.includes(searchTerm)
    );
  });

  const indexOfFirstLoanOnPage = (currentPage - 1) * loansPerPage;
  const indexOfLastLoanOnPage = currentPage * loansPerPage;
  const currentLoansOnPage = filteredLoans.slice(
    indexOfFirstLoanOnPage,
    indexOfLastLoanOnPage,
  );

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  const nextPage = () => {
    if (currentPage < Math.ceil(filteredLoans.length / loansPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  /**
   * Obsługuje zmianę liczby wypożyczeń na stronę.
   * @param {React.ChangeEvent<HTMLSelectElement>} event - Zdarzenie zmiany wartości w polu wyboru.
   */
  const handleLoansPerPageChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
  ) => {
    const newLoansPerPage = parseInt(event.target.value);
    setLoansPerPage(newLoansPerPage);
    setCurrentPage(1);
  };

  return (
    <Formik
      initialValues={{ searchTerm: '' }}
      onSubmit={(values, actions) => {
        setSearchTerm(values.searchTerm);
        setCurrentPage(1);
        actions.setSubmitting(false);
      }}
    >
      <Form>
        <div className="searchFieldContainer">
          <Field name="searchTerm">
            {(props: FieldProps<string>) => (
              <TextField
                {...props.field}
                label={t('searchLoan')}
                variant="outlined"
                InputProps={{
                  startAdornment: <SearchIcon className="iconStyle" />,
                }}
                className="searchField"
              />
            )}
          </Field>
        </div>
        <div className="loansGridContainer">
          <Grid container spacing={2} className="formField formContainer">
            {currentLoansOnPage.map((loan: Loan) => (
              <Grid item key={loan.id} xs={12} sm={6} md={4} lg={3}>
                <IndividualLoanFromList loan={loan} />
              </Grid>
            ))}
          </Grid>
        </div>
        <Grid container className="formContainer" justifyContent="center">
          <Button onClick={prevPage} disabled={currentPage === 1}>
            {t('previous')}
          </Button>
          {Array.from({
            length: Math.ceil(filteredLoans.length / loansPerPage),
          }).map((_, index) => (
            <Button
              key={index}
              onClick={() => paginate(index + 1)}
              className="searchButton"
              disabled={currentPage === index + 1}
            >
              {index + 1}
            </Button>
          ))}
          <Button
            onClick={nextPage}
            disabled={
              currentPage === Math.ceil(filteredLoans.length / loansPerPage)
            }
          >
            {t('next')}
          </Button>
        </Grid>
        <div className="loansPerPageContainer">
          <div className="loansPerPageFrame">
            <AutoStoriesIcon className="loansPerPageIcon" />
            <label htmlFor="loansPerPageSelect" className="loansPerPageLabel">
              {t('loansPerPage')}
            </label>
            <select
              id="loansPerPageSelect"
              value={loansPerPage}
              onChange={handleLoansPerPageChange}
              className="loansPerPageSelect"
            >
              <option value={4}>4</option>
              <option value={8}>8</option>
              <option value={12}>12</option>
            </select>
          </div>
        </div>
      </Form>
    </Formik>
  );
}

export default ListOfLoans;
