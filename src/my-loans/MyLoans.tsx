import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useApi } from '../api/ApiProvider';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Typography,
  Paper,
  Grid,
  TextField,
  Button,
  Tooltip,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import './MyLoans.css';
import { User } from '../list-of-users/IndividualUserFromList';
import { Book } from '../list-of-books/IndividualBookFromList';
import HomeIcon from '@mui/icons-material/Home';
import { useNavigate } from 'react-router-dom';

/**
 * Interfejs reprezentujący wypożyczenie.
 */
interface Loan {
  id: number;
  user: User;
  book: Book;
  loanDate: string;
  deadlineDate: string;
  returnDate: string | null;
}
/**
 * Komponent odpowiedzialny za wyświetlanie listy wypożyczeń użytkownika.
 * Umożliwia wyszukiwanie, paginację oraz zmianę liczby wypożyczeń na stronę.
 */
export default function MyLoans() {
  const { t } = useTranslation();
  const apiClient = useApi();
  const [loans, setLoans] = useState<Loan[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [loansPerPage, setLoansPerPage] = useState<number>(8);
  const navigate = useNavigate();

  useEffect(() => {
    /**
     * Funkcja odpowiedzialna za pobranie danych wypożyczeń użytkownika z API i aktualizację stanu komponentu.
     */
    const fetchLoans = async () => {
      try {
        const response = await apiClient.getUserLoans();
        if (response.success) {
          setLoans(response.data || []);
        } else {
          toast.error(t('loadingError'));
        }
      } catch (error) {
        toast.error(t('loadingError'));
      }
    };

    fetchLoans();
  }, [apiClient, t]);
  /**
   * Funkcja formatująca podany ciąg daty do formatu lokalnego.
   * @param date - Ciąg daty do sformatowania.
   * @returns Sformatowany ciąg daty.
   */
  const formatDate = (date: string): string => {
    const formattedDate = new Date(date).toLocaleDateString();
    return formattedDate;
  };
  /**
   * Funkcja określająca nazwę klasy CSS na podstawie tego, czy pożyczka została zwrócona.
   * @param returnDate - Ciąg daty zwrotu lub null.
   * @returns Nazwa klasy CSS ('loan-item not-returned' lub 'loan-item returned').
   */
  const getPaperClassName = (returnDate: string | null): string => {
    if (!returnDate) {
      return 'loan-item not-returned';
    } else {
      return 'loan-item returned';
    }
  };
  /**
   * Filtruje wypożyczenia na podstawie wprowadzonego terminu wyszukiwania.
   * @param loan - Wypożyczenie do sprawdzenia.
   * @returns True, jeśli wypożyczenie pasuje do wprowadzonego terminu wyszukiwania, w przeciwnym razie false.
   */
  const filteredLoans = loans.filter((loan: Loan) => {
    const loanDate = new Date(loan.loanDate).toLocaleDateString();
    const deadlineDate = new Date(loan.deadlineDate).toLocaleDateString();

    return (
      loan.user.fullUserName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loan.book.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loan.book.author.toLowerCase().includes(searchTerm.toLowerCase()) ||
      loan.book.id.toString().includes(searchTerm.toLowerCase()) ||
      loan.id.toString().includes(searchTerm.toLowerCase()) ||
      loanDate.includes(searchTerm) ||
      deadlineDate.includes(searchTerm)
    );
  });

  const indexOfFirstLoanOnPage = (currentPage - 1) * loansPerPage;
  const indexOfLastLoanOnPage = currentPage * loansPerPage;
  const currentLoansOnPage = filteredLoans.slice(
    indexOfFirstLoanOnPage,
    indexOfLastLoanOnPage,
  );

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  const nextPage = () => {
    if (currentPage < Math.ceil(filteredLoans.length / loansPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  /**
   * Obsługa zmiany wartości wyszukiwania.
   * @param event - Zdarzenie zmiany wartości w polu wyszukiwania.
   */
  const handleSearchTermChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setSearchTerm(event.target.value);
    setCurrentPage(1);
  };

  /**
   * Obsługa zmiany liczby wypożyczeń na stronę.
   * @param event - Zdarzenie zmiany liczby wypożyczeń na stronę.
   */
  const handleLoansPerPageChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
  ) => {
    const newLoansPerPage = parseInt(event.target.value);
    setLoansPerPage(newLoansPerPage);
    setCurrentPage(1);
  };

  return (
    <Box className="my-loans-container">
      <img
        src={process.env.PUBLIC_URL + '/myLoan.png'}
        alt={t('loans')}
        className="myloan-image"
      />
      <h1>{t('my_loans')}</h1>

      <TextField
        value={searchTerm}
        onChange={handleSearchTermChange}
        label={t('searchLoan')}
        variant="outlined"
        InputProps={{
          startAdornment: <SearchIcon className="iconStyle" />,
        }}
        className="searchField"
      />

      {/* Add spacing div */}
      <div className="spacing-div" />

      <Grid container spacing={2} className="formField formContainer">
        {currentLoansOnPage.map((loan: Loan) => (
          <Grid item key={loan.id} xs={12} sm={6} md={4} lg={3}>
            <Paper className={getPaperClassName(loan.returnDate)}>
              <Typography variant="body1">{`${t('loanId')}: ${loan.id}`}</Typography>
              <Typography variant="body1">
                <strong>{`${t('bookId')}: ${loan.book.id}`}</strong>
              </Typography>
              <Typography variant="body1">{`${t('loanDate')}: ${formatDate(loan.loanDate)}`}</Typography>
              <Typography variant="body1">{`${t('returnDate')}: ${loan.returnDate ? formatDate(loan.returnDate) : t('notReturned')}`}</Typography>
              <Typography variant="body1">
                <strong>{`${t('deadlineDate')}: ${formatDate(loan.deadlineDate)}`}</strong>
              </Typography>
            </Paper>
          </Grid>
        ))}
      </Grid>

      <Grid container className="formContainer" justifyContent="center">
        <Button onClick={prevPage} disabled={currentPage === 1}>
          {t('previous')}
        </Button>
        {Array.from({
          length: Math.ceil(filteredLoans.length / loansPerPage),
        }).map((_, index) => (
          <Button
            key={index}
            onClick={() => paginate(index + 1)}
            className="searchButton"
            disabled={currentPage === index + 1}
          >
            {index + 1}
          </Button>
        ))}
        <Button
          onClick={nextPage}
          disabled={
            currentPage === Math.ceil(filteredLoans.length / loansPerPage)
          }
        >
          {t('next')}
        </Button>
      </Grid>

      <div className="loansPerPageContainer">
        <div className="loansPerPageFrame">
          <AutoStoriesIcon className="loansPerPageIcon" />
          <label htmlFor="loansPerPageSelect" className="loansPerPageLabel">
            {t('loansPerPage')}
          </label>
          <select
            id="loansPerPageSelect"
            value={loansPerPage}
            onChange={handleLoansPerPageChange}
            className="loansPerPageSelect"
          >
            <option value={4}>4</option>
            <option value={8}>8</option>
            <option value={12}>12</option>
          </select>
        </div>
        <Tooltip title={t('backToHome')}>
          <HomeIcon
            className="back-to-home-icon"
            onClick={() => navigate('/home')}
          />
        </Tooltip>
      </div>

      <ToastContainer />
    </Box>
  );
}
