import React, { useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';

import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './DeleteBook.css';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
/**
 * Komponent strony usuwania książki.
 * Umożliwia użytkownikowi usunięcie książki poprzez formularz.
 */
export default function DeleteBookPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [bookId, setBookId] = useState('');
  const [buttonColor, setButtonColor] = useState('grey');

  const handleDelete = async () => {
    try {
      const response = await apiClient.deleteBook(bookId);
      if (response.success) {
        toast.success(t('delete_success'));
        navigate('/list-of-books');
      } else {
        toast.error(t('delete_failed'));
      }
    } catch (error) {
      toast.error(t('delete_failed'));
    }
  };
  const handleClick = () => {
    setButtonColor('red'); //zmiana koloru przycisku na czerwony po kliknięciu
    handleDelete();
  };

  return (
    <Box className="delete-book-container">
      <img
        src={process.env.PUBLIC_URL + '/deleteBook.png'}
        alt={t('addBookAlt')}
        className="delete-book-image"
      />
      <Typography variant="h2" className="delete-book-title">
        {t('delete_book')}
        <RemoveCircleOutlineIcon style={{ marginRight: '4px' }} />
      </Typography>

      <TextField
        label={t('book_id')}
        value={bookId}
        onChange={(e) => setBookId(e.target.value)}
        className="delete-book-input"
        variant="outlined"
      />
      <Button
        variant="contained"
        style={{ backgroundColor: buttonColor, color: '#fff' }}
        onClick={handleClick}
        className="delete-book-button"
      >
        {t('delete_book_button')}
      </Button>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
