import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useApi } from '../api/ApiProvider';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { User } from '../list-of-users/IndividualUserFromList';
import { Box, Tooltip, Typography } from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import './AccountInfo.css';
import InfoIcon from '@mui/icons-material/Info';
/**
 * Komponent strony zawierającej informacje o koncie zalogowanego użytkownika.
 * Umożliwia użytkownikom zobaczenie informacji takich jak: nazwa użytkownika, rola, pełna nazwa uzytkownika i email
 */
export default function AccountInfoPage() {
  const { t } = useTranslation();
  const apiClient = useApi();
  const navigate = useNavigate();
  const [userInfo, setUserInfo] = useState<User | null>(null);

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await apiClient.getCurrentUser();
        if (response.success && response.data) {
          setUserInfo(response.data);
        } else {
          toast.error(t('loading'));
        }
      } catch (error) {
        toast.error(t('loading'));
      }
    };

    fetchUserInfo();
  }, [apiClient, t]);

  return (
    <Box className="account-info-container">
      <img
        src={process.env.PUBLIC_URL + '/accountInfo.png'}
        alt={t('accountInfo')}
        className="account-info-image"
      />
      <Typography variant="h1" className="account-info-title">
        {t('account_info')}
        <InfoIcon style={{ fontSize: '2.5rem', marginRight: '4px' }} />
      </Typography>

      <Box className="account-info-content">
        {userInfo ? (
          <Box>
            <Box className="account-info-item">
              <Typography variant="body1" className="account-info-label">
                {t('username')}:
              </Typography>
              <Typography variant="body1" className="account-info-value">
                {userInfo.userName}
              </Typography>
            </Box>
            <Box className="account-info-item">
              <Typography variant="body1" className="account-info-label">
                {t('email')}:
              </Typography>
              <Typography variant="body1" className="account-info-value">
                {userInfo.email}
              </Typography>
            </Box>
            <Box className="account-info-item">
              <Typography variant="body1" className="account-info-label">
                {t('full_name')}:
              </Typography>
              <Typography variant="body1" className="account-info-value">
                {userInfo.fullUserName}
              </Typography>
            </Box>
            <Box className="account-info-item">
              <Typography variant="body1" className="account-info-label">
                {t('role')}:
              </Typography>
              <Typography variant="body1" className="account-info-value">
                {userInfo.role}
              </Typography>
            </Box>
          </Box>
        ) : (
          <Typography variant="body1" className="account-info-loading">
            {t('loading')}
          </Typography>
        )}
      </Box>

      <ToastContainer />
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
