import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { LoginDto, LoginResponseDto } from './dto/login.dto';
import { Book } from '../list-of-books/IndividualBookFromList';
import { NewbookDto } from './dto/newbook.dto';
import { NewloanDto } from './dto/newloan.dto';
import { NewuserDto } from './dto/newuser.dto';
import { User } from '../list-of-users/IndividualUserFromList';
import { Loan } from '../list-of-loans/IndividualLoanFromList';
/**
 * Typ używany do zwracania odpowiedzi przez klienta biblioteki.
 *
 * @template T Typ danych zwracanych przez odpowiedź.
 */
export type ClientResponse<T> = {
  success: boolean;
  data: T;
  statusCode: number;
};

/**
 * Klasa `LibraryClient` służy do komunikacji z serwerem biblioteki.
 */
export class LibraryClient {
  private client: AxiosInstance;
  private token: string | null = null;

  constructor() {
    this.client = axios.create({
      baseURL: 'http://localhost:8080',
    });

    this.client.interceptors.request.use((config) => {
      if (this.token) {
        config.headers['Authorization'] = `Bearer ${this.token}`;
      }
      console.log('Request headers:', config.headers); //w konsoli logowanie nagłówków
      return config;
    });
  }

  /**
   * Loguje użytkownika do systemu.
   *
   * @param {LoginDto} data Dane logowania użytkownika.
   * @returns {Promise<ClientResponse<LoginResponseDto | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async login(
    data: LoginDto,
  ): Promise<ClientResponse<LoginResponseDto | null>> {
    try {
      const response: AxiosResponse<any> = await this.client.post(
        '/login',
        data,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      console.log('Response from login:', response.data); //w konsoli logowanie odpowiedzi

      this.token = response.data || null; //ustawienie tokena bezpośrednio z ciała odpowiedzi

      console.log('Token received:', this.token); //w konsoli logowanietokena
      return {
        success: true,
        data: { token: response.data }, //zwracanie tokena w obiekcie odpowiedzi
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Pobiera listę wszystkich książek z serwera.
   *
   * @returns {Promise<ClientResponse<any | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async getBooks(): Promise<ClientResponse<any | null>> {
    try {
      const response = await this.client.get('/book/getAll');
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Pobiera listę wszystkich wypożyczeń z serwera.
   *
   * @returns {Promise<ClientResponse<any | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async getLoans(): Promise<ClientResponse<any | null>> {
    try {
      const response = await this.client.get('/loan/getAll');
      console.log('Request headers for getLoans:', response.config.headers); // Logowanie nagłówków
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      console.log('Error in getLoans:', axiosError.response?.data); // Logowanie błędu
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Pobiera listę wszystkich użytkowników z serwera.
   *
   * @returns {Promise<ClientResponse<any | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async getUsers(): Promise<ClientResponse<any | null>> {
    try {
      const response = await this.client.get('/user/getAll');
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Dodaje nową książkę do serwera.
   *
   * @param {NewbookDto} data Dane nowej książki.
   * @returns {Promise<ClientResponse<Book | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async addBook(data: NewbookDto): Promise<ClientResponse<Book | null>> {
    try {
      const response = await this.client.post('/book/add', data, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Dodaje nowe wypożyczenie do serwera.
   *
   * @param {NewloanDto} data Dane nowego wypożyczenia.
   * @returns {Promise<ClientResponse<Loan | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async addLoan(data: NewloanDto): Promise<ClientResponse<Loan | null>> {
    try {
      const response = await this.client.post('/loan/add', data, {
        headers: {
          'Content-Type': 'application/json',
        },
      }); //wysłanie żądanie dodania wypożyczenia do serwera
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Dodaje nowego użytkownika do serwera.
   *
   * @param {NewuserDto} data Dane nowego użytkownika.
   * @returns {Promise<ClientResponse<User | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async addUser(data: NewuserDto): Promise<ClientResponse<User | null>> {
    try {
      console.log('Request data for addBook:', data); // Logowanie danych wysyłanych do serwera
      const response = await this.client.post('/user/add', data, {
        headers: {
          'Content-Type': 'application/json',
        },
      }); //wysłanie żądanie dodania użytkownika do serwera
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Usuwa książkę z serwera na podstawie podanego id książki
   *
   * @param {string} bookId Identyfikator książki do usunięcia.
   * @returns {Promise<ClientResponse<void>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async deleteBook(bookId: string): Promise<ClientResponse<void>> {
    try {
      const response = await this.client.delete(`book/delete/${bookId}`);
      return {
        success: true,
        data: undefined,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: undefined,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Usuwa użytkownika z serwera na podstawie podanego id użytkownika
   *
   * @param {string} userId Identyfikator użytkownika do usunięcia.
   * @returns {Promise<ClientResponse<void>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async deleteUser(userId: string): Promise<ClientResponse<void>> {
    try {
      const response = await this.client.delete(`user/delete/${userId}`);
      return {
        success: true,
        data: undefined,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: undefined,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Zwraca wypożyczenie na serwerze.
   *
   * @param {number} loanId Identyfikator wypożyczenia do zwrócenia.
   * @param {string} returnDate Data zwrotu książki.
   * @returns {Promise<ClientResponse<Loan | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async returnLoan(
    loanId: number,
    returnDate: string,
  ): Promise<ClientResponse<Loan | null>> {
    try {
      const response = await this.client.put(`/loan/return/${loanId}`, null, {
        params: { returnDate },
      });
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Pobiera informacje o zalogowanym użytkowniku.
   *
   * @returns {Promise<ClientResponse<User | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async getCurrentUser(): Promise<ClientResponse<User | null>> {
    try {
      const response = await this.client.get('/user/InfoAboutMe');
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Aktualizuje dane użytkownika na serwerze.
   *
   * @param {User} userData Dane użytkownika do aktualizacji.
   * @returns {Promise<ClientResponse<User | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async updateUser(
    userData: Partial<User>,
  ): Promise<ClientResponse<User | null>> {
    try {
      const response = await this.client.put('/user/update', userData, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }
  /**
   * Pobiera listę wypożyczeń zalogowanego użytkownika z serwera.
   *
   * @returns {Promise<ClientResponse<Loan[] | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async getUserLoans(): Promise<ClientResponse<Loan[]>> {
    try {
      const response = await this.client.get('/loan/myLoans');
      return {
        success: true,
        data: response.data || [],
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: [],
        statusCode: axiosError.response?.status || 0,
      };
    }
  }

  /**
   * Aktualizuje dane książki na serwerze.
   *
   * @param {number} bookId Identyfikator książki do aktualizacji.
   * @param {Partial<Book>} bookData Dane książki do aktualizacji.
   * @returns {Promise<ClientResponse<Book | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async updateBook(
    bookId: number,
    bookData: Partial<Book>,
  ): Promise<ClientResponse<Book | null>> {
    try {
      const response = await this.client.put(
        `/book/updateBookDetails/${bookId}`,
        bookData,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }
  /**
   * Aktualizuje dane użytkownika na serwerze.
   *
   * @param {number} userId Identyfikator użytkownika do aktualizacji.
   * @param {Partial<User>} userData Dane użytkownika do aktualizacji.
   * @returns {Promise<ClientResponse<User | null>>} Obiekt zawierający status operacji, dane odpowiedzi i kod statusu.
   */
  public async updateUserLibrarian(
    userId: number,
    userData: Partial<User>,
  ): Promise<ClientResponse<User | null>> {
    try {
      const response = await this.client.put(
        `/user/updateOther/${userId}`,
        userData,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      return {
        success: true,
        data: response.data,
        statusCode: response.status,
      };
    } catch (error) {
      const axiosError = error as AxiosError<Error>;
      return {
        success: false,
        data: null,
        statusCode: axiosError.response?.status || 0,
      };
    }
  }
}
