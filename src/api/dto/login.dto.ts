export class LoginDto {
  login: string | null | undefined;
  password: string | null | undefined;
}

export class LoginResponseDto {
  token: string | null | undefined;
}
