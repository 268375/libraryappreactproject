export interface NewuserDto {
  userName: string;
  password: string;
  role: string;
  email: string;
  fullUserName: string;
}
