export interface UserLoan {
  id: string;
}

export interface BookLoan {
  id: string;
}

export interface NewloanDto {
  user: UserLoan;
  book: BookLoan;
  loanDate: string;
  deadlineDate: string;
  returnDate: string | null;
}
