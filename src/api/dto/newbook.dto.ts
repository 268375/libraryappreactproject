export interface NewbookDto {
  isbn: string;
  title: string;
  author: string;
  publisher: string;
  publishYear: string;
  availableCopies: string;
}
