import { LibraryClient } from './library-client';
import { createContext, useContext } from 'react';
/**
 * Kontekst dla klienta API.
 * Umożliwia przekazywanie instancji LibraryClient przez drzewo komponentów
 */
const ApiContext = createContext(new LibraryClient());
/**
 * Komponent ApiProvider,który zapewnia nową instancję LibraryClient wszystkim komponentom potomnym.
 *
 * @param {object} props - Obiekt właściwości.
 * @param {React.ReactNode} props.children - Komponenty potomne
 * @returns {JSX.Element} Komponent ApiContext
 */
export default function ApiProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const apiClient = new LibraryClient();
  return (
    <ApiContext.Provider value={apiClient}>{children}</ApiContext.Provider>
  );
}
/**
 * Hook useApi.
 * Zapewnia dostęp do instancji LibraryClient za pośrednictwem ApiContext.
 *
 * @returns {LibraryClient} aktualna wartość ApiContext.
 */
export function useApi() {
  return useContext(ApiContext);
}
