import React, { useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';

import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './DeleteUser.css';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
/**
 * Komponent strony usuwania użytkownika.
 * Umożliwia użytkownikowi usunięcie użytkownika poprzez formularz.
 */
export default function DeleteUserPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [userId, setUserId] = useState('');
  const [buttonColor, setButtonColor] = useState('grey');

  const handleDelete = async () => {
    try {
      const response = await apiClient.deleteUser(userId);
      if (response.success) {
        toast.success(t('delete_user_success'));
        navigate('/list-of-users');
      } else {
        toast.error(t('delete_user_failed'));
      }
    } catch (error) {
      toast.error(t('delete_user_failed'));
    }
  };
  const handleClick = () => {
    setButtonColor('red'); //zmiana koloru przycisku na czerwony po kliknięciu
    handleDelete();
  };

  return (
    <Box className="delete-user-container">
      <img
        src={process.env.PUBLIC_URL + '/deleteUser.png'}
        alt={t('deleteUserAlt')}
        className="delete-user-image"
      />
      <Typography variant="h2" className="delete-user-title">
        {t('delete_user')}
        <RemoveCircleOutlineIcon style={{ marginRight: '4px' }} />
      </Typography>

      <TextField
        label={t('user_id')}
        value={userId}
        onChange={(e) => setUserId(e.target.value)}
        className="delete-user-input"
        variant="outlined"
      />
      <Button
        variant="contained"
        style={{ backgroundColor: buttonColor, color: '#fff' }}
        onClick={handleClick}
        className="delete-user-button"
      >
        {t('delete_user_button')}
      </Button>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
