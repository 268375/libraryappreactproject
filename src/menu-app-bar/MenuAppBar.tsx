import React from 'react';
import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
  Tooltip,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import LogoutIcon from '@mui/icons-material/Logout';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useNavigate } from 'react-router-dom';
import './MenuAppBar.css';
import { useTranslation } from 'react-i18next';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
/**
 * Komponent reprezentujący pasek menu aplikacji.
 */
export default function MenuAppBar() {
  const navigate = useNavigate();
  const { t, i18n } = useTranslation();
  //pobranie tokena z localStorage
  const token = localStorage.getItem('token');
  //sprawdzenie czy token istnieje i czy zawiera informacje o roli użytkownika
  const role = token ? JSON.parse(atob(token.split('.')[1])).role : '';

  //do kontrolowania otwierania i zamykania menu.
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  /**
   * Obsługuje otwarcie menu.
   * @param {React.MouseEvent<HTMLButtonElement>} event - Zdarzenie kliknięcia przycisku.
   */
  const handleMenuOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  /**
   * Obsługuje zamknięcie menu.
   */
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  /**
   * Obsługuje kliknięcie na pozycję menu.
   * @param {string} path - Ścieżka do nawigacji.
   */
  const handleMenuItemClick = (path: string) => {
    navigate(path);
    handleMenuClose();
  };
  //stan dla menu konta
  const [accountAnchorEl, setAccountAnchorEl] =
    React.useState<null | HTMLElement>(null);

  // Funkcja do obsługi otwarcia menu konta
  const handleAccountMenuOpen = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    setAccountAnchorEl(event.currentTarget);
  };

  // Funkcja do obsługi zamknięcia menu konta
  const handleAccountMenuClose = () => {
    setAccountAnchorEl(null);
  };
  //stan dla przycisku zmiany języka oraz funkcje do jego otwierania i zamykania
  const [languageAnchorEl, setLanguageAnchorEl] =
    React.useState<null | HTMLElement>(null);
  /**
   * Obsługuje otwarcie menu wyboru języka.
   * @param {React.MouseEvent<HTMLButtonElement>} event - Zdarzenie kliknięcia przycisku.
   */
  const handleLanguageMenuOpen = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    setLanguageAnchorEl(event.currentTarget);
  };
  /**
   * Obsługuje zamknięcie menu wyboru języka.
   */
  const handleLanguageMenuClose = () => {
    setLanguageAnchorEl(null);
  };

  /**
   * Obsługuje kliknięcie na opcję wyboru języka.
   * @param {string} language - Wybrany język.
   */
  const handleLanguageItemClick = (language: string) => {
    i18n.changeLanguage(language);
    handleLanguageMenuClose();
  };

  return (
    <AppBar
      position="static"
      sx={{ backgroundColor: '#f0f0f0' }}
      className="appbar"
    >
      <Toolbar>
        <Tooltip title={t('menu')}>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenuOpen}
            sx={{ color: '#000' }}
            className="menu-button"
          >
            <MenuIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title={t('account')}>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="account"
            aria-controls="account"
            aria-haspopup="true"
            onClick={handleAccountMenuOpen}
            sx={{ color: '#000' }}
            className="account-button"
          >
            <ManageAccountsIcon />
          </IconButton>
        </Tooltip>
        <Menu
          id="account-menu-appbar"
          anchorEl={accountAnchorEl}
          open={Boolean(accountAnchorEl)}
          onClose={handleAccountMenuClose}
        >
          <MenuItem onClick={() => handleMenuItemClick('/account-settings')}>
            {t('change_account_settings')}
          </MenuItem>
          <MenuItem onClick={() => handleMenuItemClick('/account-info')}>
            {t('view_account_info')}
          </MenuItem>
        </Menu>
        <Typography
          variant="h6"
          component="div"
          sx={{ flexGrow: 1, color: '#000' }}
        ></Typography>
        <Box>
          <Tooltip title={t('change_language')}>
            <IconButton
              size="large"
              color="inherit"
              aria-label="language"
              aria-controls="language-menu-appbar"
              aria-haspopup="true"
              onClick={handleLanguageMenuOpen}
              sx={{ color: '#000' }}
              className="language-button"
            >
              <Typography variant="body1" sx={{ marginRight: '5px' }}>
                {t('language')}
              </Typography>
              <ExpandMoreIcon />
            </IconButton>
          </Tooltip>
          <Menu
            id="language-menu-appbar"
            anchorEl={languageAnchorEl}
            open={Boolean(languageAnchorEl)}
            onClose={handleLanguageMenuClose}
          >
            <MenuItem onClick={() => handleLanguageItemClick('en')}>
              English
            </MenuItem>
            <MenuItem onClick={() => handleLanguageItemClick('pl')}>
              Polski
            </MenuItem>
          </Menu>
          <Tooltip title={t('log_out')}>
            <IconButton
              size="large"
              color="inherit"
              aria-label="account"
              onClick={() => navigate('/login')}
              sx={{ color: '#000' }}
              className="logout-button"
            >
              <LogoutIcon />
            </IconButton>
          </Tooltip>
        </Box>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
        >
          {role === 'ROLE_LIBRARIAN'
            ? [
                <MenuItem
                  key="list-of-books"
                  onClick={() => handleMenuItemClick('/list-of-books')}
                >
                  {t('list_of_books')}
                </MenuItem>,
                <MenuItem
                  key="add-new-book"
                  onClick={() => handleMenuItemClick('/add-new-book')}
                >
                  {t('add_new_book')}
                </MenuItem>,
                <MenuItem
                  key="edit-book"
                  onClick={() => handleMenuItemClick('/edit-book')}
                >
                  {t('editBook')}
                </MenuItem>,
                <MenuItem
                  key="delete-book"
                  onClick={() => handleMenuItemClick('/delete-book')}
                >
                  {t('delete_book')}
                </MenuItem>,
                <MenuItem
                  key="list-of-loans"
                  onClick={() => handleMenuItemClick('/list-of-loans')}
                >
                  {t('list_of_loans')}
                </MenuItem>,
                <MenuItem
                  key="add-new-loan"
                  onClick={() => handleMenuItemClick('/add-new-loan')}
                >
                  {t('add_new_loan')}
                </MenuItem>,
                <MenuItem
                  key="return-loan"
                  onClick={() => handleMenuItemClick('/return-loan')}
                >
                  {t('return_loan')}
                </MenuItem>,
                <MenuItem
                  key="list-of-users"
                  onClick={() => handleMenuItemClick('/list-of-users')}
                >
                  {t('list_of_users')}
                </MenuItem>,
                <MenuItem
                  key="add-new-user"
                  onClick={() => handleMenuItemClick('/add-new-user')}
                >
                  {t('add_new_user')}
                </MenuItem>,
                <MenuItem
                  key="edit-user"
                  onClick={() => handleMenuItemClick('/edit-user')}
                >
                  {t('editUser')}
                </MenuItem>,
                <MenuItem
                  key="delete-user"
                  onClick={() => handleMenuItemClick('/delete-user')}
                >
                  {t('delete_user')}
                </MenuItem>,
              ]
            : [
                <MenuItem
                  key="list-of-books"
                  onClick={() => handleMenuItemClick('/list-of-books')}
                >
                  {t('list_of_books')}
                </MenuItem>,
                <MenuItem
                  key="my-loans"
                  onClick={() => handleMenuItemClick('/my-loans')}
                >
                  {t('my_loans')}
                </MenuItem>,
              ]}
        </Menu>
      </Toolbar>
    </AppBar>
  );
}
