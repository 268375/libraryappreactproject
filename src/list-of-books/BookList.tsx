/**
 * Komponent wyświetlający listę książek z funkcjami filtrowania, paginacji oraz
 * możliwością zmiany liczby książek na stronie.
 */
import React, { useState } from 'react';
import {
  Grid,
  TextField,
  Button,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import IndividualBookFromList from './IndividualBookFromList';
import { Formik, Form, Field, FieldProps } from 'formik';
import './BookList.css';
import { useTranslation } from 'react-i18next';
/**
 * Interfejs reprezentujący książkę.
 */
interface Book {
  id: number;
  isbn: string;
  title: string;
  author: string;
  publisher: string;
  publishYear: number;
  availableCopies: number;
}
/**
 * Interfejs reprezentujący właściwości komponentu BookList.
 */
interface BookListProps {
  books: Book[];
}

function BookList({ books }: BookListProps) {
  const { t } = useTranslation();
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [searchCategory, setSearchCategory] = useState<string>('title');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [booksPerPage, setBooksPerPage] = useState<number>(8);
  /**
   * Filtruje książki na podstawie wybranej kategorii i terminu wyszukiwania.
   *
   * @returns {Book[]} Zwraca przefiltrowaną listę książek.
   */
  const filteredBooks = books.filter((book: Book) => {
    switch (searchCategory) {
      case 'author':
        return book.author.toLowerCase().includes(searchTerm.toLowerCase());
      case 'title':
        return book.title.toLowerCase().includes(searchTerm.toLowerCase());
      case 'publisher':
        return book.publisher.toLowerCase().includes(searchTerm.toLowerCase());
      case 'isbn':
        return book.isbn.toLowerCase().includes(searchTerm.toLowerCase());
      case 'publishYear':
        return book.publishYear.toString().includes(searchTerm);
      default:
        return true;
    }
  });

  const indexOfFirstBookOnPage = (currentPage - 1) * booksPerPage;
  const indexOfLastBookOnPage = currentPage * booksPerPage;
  const currentBooksOnPage = filteredBooks.slice(
    indexOfFirstBookOnPage,
    indexOfLastBookOnPage,
  );
  /**
   * Ustawia bieżącą stronę na wybraną przez użytkownika.
   *
   * @param {number} pageNumber Numer strony do ustawienia.
   */
  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  /**
   * Przechodzi do następnej strony.
   */
  const nextPage = () => {
    if (currentPage < Math.ceil(filteredBooks.length / booksPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };
  /**
   * Przechodzi do poprzedniej strony.
   */
  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  /**
   * Obsługuje zmianę liczby książek na stronie.
   *
   * @param {React.ChangeEvent<HTMLSelectElement>} event Zdarzenie zmiany wyboru.
   */
  const handleBooksPerPageChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
  ) => {
    const newBooksPerPage = parseInt(event.target.value);
    setBooksPerPage(newBooksPerPage);
    setCurrentPage(1);
  };

  return (
    <Formik
      initialValues={{ searchTerm: '', searchCategory: 'title' }}
      onSubmit={(values, actions) => {
        setSearchTerm(values.searchTerm);
        setSearchCategory(values.searchCategory);
        setCurrentPage(1);
        actions.setSubmitting(false);
      }}
    >
      {({ values, handleChange }) => (
        <Form>
          <div className="searchFieldContainer">
            <FormControl variant="outlined" className="categorySelect">
              <InputLabel>{t('searchCategory')}</InputLabel>
              <Select
                name="searchCategory"
                value={values.searchCategory}
                onChange={handleChange}
                label={t('searchCategory')}
                className="searchField"
              >
                <MenuItem value="title">{t('title')}</MenuItem>
                <MenuItem value="author">{t('author')}</MenuItem>
                <MenuItem value="publisher">{t('publisher')}</MenuItem>
                <MenuItem value="isbn">ISBN</MenuItem>
                <MenuItem value="publishYear">{t('publishYear')}</MenuItem>
              </Select>
            </FormControl>
            <Field name="searchTerm">
              {(props: FieldProps<string>) => (
                <TextField
                  {...props.field}
                  label={t('searchBook')}
                  variant="outlined"
                  InputProps={{
                    startAdornment: <SearchIcon className="iconStyle" />,
                  }}
                  className="searchField"
                />
              )}
            </Field>
            <Button
              type="submit"
              variant="contained"
              style={{ backgroundColor: 'lawngreen', color: 'black' }}
              className="searchButtonBook"
            >
              {t('search')}
            </Button>
          </div>
          <div className="booksGridContainer">
            <Grid container spacing={2} className="formField formContainer">
              {currentBooksOnPage.map((book: Book) => (
                <Grid item key={book.id} xs={12} sm={6} md={4} lg={3}>
                  <IndividualBookFromList book={book} />
                </Grid>
              ))}
            </Grid>
          </div>
          <Grid container className="formContainer" justifyContent="center">
            <Button onClick={prevPage} disabled={currentPage === 1}>
              {t('previous')}
            </Button>
            {Array.from({
              length: Math.ceil(filteredBooks.length / booksPerPage),
            }).map((_, index) => (
              <Button
                key={index}
                onClick={() => paginate(index + 1)}
                className="searchButton"
                disabled={currentPage === index + 1}
              >
                {index + 1}
              </Button>
            ))}
            <Button
              onClick={nextPage}
              disabled={
                currentPage === Math.ceil(filteredBooks.length / booksPerPage)
              }
            >
              {t('next')}
            </Button>
          </Grid>
          <div className="booksPerPageContainer">
            <div className="booksPerPageFrame">
              <AutoStoriesIcon className="booksPerPageIcon" />
              <label htmlFor="booksPerPageSelect" className="booksPerPageLabel">
                {t('booksPerPage')}
              </label>
              <select
                id="booksPerPageSelect"
                value={booksPerPage}
                onChange={handleBooksPerPageChange}
                className="booksPerPageSelect"
              >
                <option value={4}>4</option>
                <option value={8}>8</option>
                <option value={12}>12</option>
              </select>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default BookList;
