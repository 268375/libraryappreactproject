import React, { useState, useEffect } from 'react';
import BookList from './BookList';
import './MainComponent.css';
import { useNavigate } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import { Tooltip } from '@mui/material';
import { Book } from './IndividualBookFromList';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent główny do wyświetlania listy z książkami znajdującymi się w bibliotece.
 *
 * @component
 * @returns {JSX.Element} JSX kod renderujący główny komponent aplikacji.
 */
function MainComponent() {
  const { t } = useTranslation();
  const [books, setBooks] = useState<Book[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const navigate = useNavigate();
  const apiClient = useApi();
  /**
   * Efekt pobierający listę książek z API.
   */
  useEffect(() => {
    /**
     * Funkcja asynchroniczna pobierająca listę książek z API.
     */
    const fetchBooks = async () => {
      const response = await apiClient.getBooks();
      if (response.success) {
        setBooks(response.data);
      } else {
        setError(t('failedToDownloadBooks'));
      }
      setLoading(false);
    };

    fetchBooks();
  }, [t]);
  /**
   * Renderuje komponent w trakcie ładowania.
   * @returns {JSX.Element} Komponent informacji o ładowaniu.
   */
  if (loading) {
    return <div>{t('loading')}</div>;
  }
  /**
   * Renderuje komponent w przypadku wystąpienia błędu.
   * @returns {JSX.Element} Komponent informacji o błędzie.
   */
  if (error) {
    return <div>{`${t('error')}: ${error}`}</div>;
  }
  /**
   * Renderuje komponent aplikacji odpowiedzialny za wyświetlanie listy książek w bibliotece.
   * @returns {JSX.Element} JSX kod renderujący główny komponent aplikacji.
   */
  return (
    <div className="main-container">
      <img
        src={process.env.PUBLIC_URL + '/books.jpg'}
        alt={t('booksAltText')}
        className="books-image"
      />
      <h1>{t('listOfBooks')}</h1>
      <BookList books={books} />
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </div>
  );
}

export default MainComponent;
