import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';
import './IndividualBookFromList.css';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
/**
 * Interfejs Book definiujący strukturę obiektu książki.
 * @typedef {Object} Book
 * @property {number} id - ID książki.
 * @property {string} isbn - ISBN książki.
 * @property {string} title - Tytuł książki.
 * @property {string} author - Autor książki.
 * @property {string} publisher - Wydawca książki.
 * @property {number} publishYear - Rok wydania książki.
 * @property {number} availableCopies - Liczba dostępnych kopii książki.
 */
export interface Book {
  id: number;
  isbn: string;
  title: string;
  author: string;
  publisher: string;
  publishYear: number;
  availableCopies: number;
}
/**
 * Komponent IndividualBookFromList.
 *
 * @component
 * @param {Object} props - właściwości przekazywane do komponentu IndividualBookFromList
 * @param {Book} props.book - obiekt reprezentujący książkę
 * @returns {JSX.Element} JSX kod renderujący pojedynczą książkę na liście
 */
function IndividualBookFromList({ book }: { book: Book }) {
  const { t } = useTranslation();

  const cardClass = classNames('book-item', {
    available: book.availableCopies > 0,
    'not-available': book.availableCopies <= 0,
  });

  return (
    <Card className={cardClass}>
      <CardContent>
        <Typography color="textSecondary">
          {t('bookId')}: {book.id}
        </Typography>
        <Typography color="textSecondary" className="bold">
          <strong>ISBN: {book.isbn}</strong>
        </Typography>
        <Typography color="textSecondary" className="italic">
          {t('title')}: {book.title}
        </Typography>
        <Typography color="textSecondary">
          {t('author')}: {book.author}
        </Typography>
        <Typography color="textSecondary">
          {t('publisher')}: {book.publisher}
        </Typography>
        <Typography color="textSecondary">
          {t('publishYear')}: {book.publishYear}
        </Typography>
        <Typography color="textSecondary">
          {t('availableCopies')}: {book.availableCopies}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default IndividualBookFromList;
