import React, { useEffect, useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { Button, TextField, Typography, Box, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './UpdateUserPage.css';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
import { toast } from 'react-toastify';
import HomeIcon from '@mui/icons-material/Home';
import UpdateIcon from '@mui/icons-material/Update';
/**
 * Typ danych reprezentujący użytkownika.
 */
type User = {
  userName: string;
  fullUserName: string;
  email: string;
  role: string;
};

type UserDataKey = keyof User;
/**
 * Komponent funkcyjny do aktualizacji danych użytkownika.
 */
export default function UpdateUserPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [userId, setUserId] = useState('');
  const [userData, setUserData] = useState<User>({
    userName: '',
    fullUserName: '',
    email: '',
    role: '',
  });
  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    setIsFormValid(userId.trim() !== '');
  }, [userId]);

  /**
   * Obsługuje akcję aktualizacji danych użytkownika.
   * Tworzy zaktualizowane dane z userData i wysyła żądanie aktualizacji przez API.
   */
  const handleUpdateUser = async () => {
    try {
      const updatedData: Partial<User> = {};
      (Object.keys(userData) as UserDataKey[]).forEach((key) => {
        const value = userData[key];
        if (value !== '' && value !== undefined) {
          updatedData[key] = value;
        }
      });
      console.log('User ID:', userId);
      console.log('Updated Data:', updatedData);

      const response = await apiClient.updateUserLibrarian(
        parseInt(userId),
        updatedData,
      );
      if (response.success) {
        toast.success(t('UpdateUserSucceeded'));
        navigate('/list-of-users');
      } else {
        toast.error(t('UpdateUserFailed'));
      }
    } catch (error) {
      toast.error(t('UpdateUserFailed'));
    }
  };
  /**
   * Obsługuje zmianę wartości w polu danych użytkownika.
   * Aktualizuje odpowiednie pole w userData.
   */
  const handleInputChange = (field: UserDataKey, value: string) => {
    setUserData((prevData) => ({
      ...prevData,
      [field]: value,
    }));
  };

  return (
    <Box className="update-user-container">
      <img
        src={process.env.PUBLIC_URL + '/updateUser.png'}
        alt={t('updateUserAlt')}
        className="update-user-image"
      />
      <Typography variant="h2" className="update-user-title">
        {t('update_user')}
        <UpdateIcon style={{ marginRight: '4px' }} />
      </Typography>
      <TextField
        label={t('UserID')}
        value={userId}
        onChange={(e) => setUserId(e.target.value)}
        className="update-user-input"
        variant="outlined"
      />
      <TextField
        label={t('Username')}
        value={userData.userName}
        onChange={(e) => handleInputChange('userName', e.target.value)}
        className="update-user-input"
        variant="outlined"
      />
      <TextField
        label={t('FullUserName')}
        value={userData.fullUserName}
        onChange={(e) => handleInputChange('fullUserName', e.target.value)}
        className="update-user-input"
        variant="outlined"
      />
      <TextField
        label={t('Email')}
        value={userData.email}
        onChange={(e) => handleInputChange('email', e.target.value)}
        className="update-user-input"
        variant="outlined"
      />
      <TextField
        label={t('Role')}
        value={userData.role}
        onChange={(e) => handleInputChange('role', e.target.value)}
        className="update-user-input"
        variant="outlined"
      />
      <Button
        variant="contained"
        color={isFormValid ? 'success' : 'inherit'}
        onClick={handleUpdateUser}
        className="update-user-button"
        disabled={!isFormValid}
      >
        {t('UpdateUser')}
      </Button>
      <Tooltip title={t('backToHome')}>
        <HomeIcon
          className="back-to-home-icon"
          onClick={() => navigate('/home')}
        />
      </Tooltip>
    </Box>
  );
}
