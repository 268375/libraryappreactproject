import React, { useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {
  Button,
  TextField,
  Typography,
  Paper,
  Box,
  Tooltip,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './AddNewBook.css';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import HomeIcon from '@mui/icons-material/Home';
import { useTranslation } from 'react-i18next';
import { useApi } from '../api/ApiProvider';
/**
 * Komponent strony dodawania nowej książki.
 * Umożliwia użytkownikom dodanie nowej książki poprzez formularz.
 */
export default function AddNewBookPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const apiClient = useApi();
  const [formData, setFormData] = useState({
    isbn: '',
    title: '',
    author: '',
    publisher: '',
    publishYear: '',
    availableCopies: '',
    errors: {
      isbn: '',
      title: '',
      author: '',
      publisher: '',
      publishYear: '',
      availableCopies: '',
    },
  });
  /**
   * Funkcja sprawdzająca, czy formularz jest poprawnie wypełniony.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */

  const isFormValid = () => {
    return (
      formData.isbn.trim() !== '' &&
      formData.title.trim() !== '' &&
      formData.author.trim() !== '' &&
      formData.publisher.trim() !== '' &&
      formData.publishYear.trim() !== '' &&
      formData.availableCopies.trim() !== ''
    );
  };
  /**
   * Funkcja obsługująca zmianę wartości pól formularza.
   * @param {object} e - Obiekt zdarzenia.
   */
  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  /**
   * Funkcja obsługująca przesłanie formularza.
   * @param {object} e - Obiekt zdarzenia.
   */

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    if (validateForm()) {
      try {
        const response = await apiClient.addBook(formData); //wysłanie żądanie dodania książki do serwera
        if (response.success) {
          console.log('Book added successfully:', response.data);
          toast.success(t('bookAddedSuccessfully'));
          setFormData({
            isbn: '',
            title: '',
            author: '',
            publisher: '',
            publishYear: '',
            availableCopies: '',
            errors: {
              isbn: '',
              title: '',
              author: '',
              publisher: '',
              publishYear: '',
              availableCopies: '',
            },
          });
          navigate('/list-of-books');
        } else {
          switch (response.statusCode) {
            case 409:
              toast.error(t('bookAlreadyExists'));
              break;
            case 400:
              toast.error(t('incompleteOrInvalidInfo'));
              break;
            default:
              toast.error(t('errorAddingBook'));
          }
        }
      } catch (error) {
        console.error('Error adding book:', error);
        toast.error(t('errorAddingBook'));
      }
    }
  };

  /**
   * Funkcja walidująca formularz.
   * @returns {boolean} True, jeśli formularz jest poprawny, w przeciwnym razie False.
   */
  const validateForm = () => {
    const errors = {
      isbn: '',
      title: '',
      author: '',
      publisher: '',
      publishYear: '',
      availableCopies: '',
    };

    let isValid = true;

    //walidacja ISBN - musi mieć dokładnie 13 cyfr
    if (formData.isbn.length !== 13) {
      errors.isbn = t('isbnLengthError');
      isValid = false;
    }

    //walidacja Title - musi mieć od 1 do 100 liter
    if (formData.title.length < 1 || formData.title.length > 100) {
      errors.title = t('titleLengthError');
      isValid = false;
    }

    //walidacja Author - musi mieć od 3 do 50 liter
    if (formData.author.length < 3 || formData.author.length > 50) {
      errors.author = t('authorLengthError');
      isValid = false;
    }

    //walidacja Publisher - musi mieć od 3 do 50 liter
    if (formData.publisher.length < 3 || formData.publisher.length > 50) {
      errors.publisher = t('publisherLengthError');
      isValid = false;
    }

    //walidacja Publish Year - musi być >0 i mniejszy od roku obecnego
    const currentYear = new Date().getFullYear();
    const publishYear = parseInt(formData.publishYear);
    if (isNaN(publishYear) || publishYear < 0 || publishYear > currentYear) {
      errors.publishYear = t('publishYearError', { currentYear });
      isValid = false;
    }

    //walidacja available copies - muszą być >0
    const availableCopies = parseInt(formData.availableCopies);
    if (isNaN(availableCopies) || availableCopies < 0) {
      errors.availableCopies = t('availableCopiesError');
      isValid = false;
    }

    setFormData({
      ...formData,
      errors: errors,
    });

    return isValid;
  };

  return (
    <Paper className="container">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        style={{
          color: '#555',
          fontWeight: 'bold',
          textAlign: 'center',
          padding: '10px 0',
          borderRadius: '4px',
        }}
      >
        <Typography variant="h4" component="span">
          {t('addNewBook')}
        </Typography>
        <AddCircleOutlineIcon style={{ marginRight: '8px' }} />
      </Box>
      <img
        src={process.env.PUBLIC_URL + '/addBook.png'}
        alt={t('addBookAlt')}
        style={{
          position: 'absolute',
          top: 150,
          left: 700,
          width: '270px',
          height: '270px',
        }}
      />
      <form className="form" onSubmit={handleSubmit}>
        <TextField
          className="form-field"
          name="isbn"
          label={t('isbnLabel')}
          value={formData.isbn}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.isbn ? true : false}
          helperText={formData.errors.isbn}
        />
        <TextField
          className="form-field"
          name="title"
          label={t('titleLabel')}
          value={formData.title}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.title ? true : false}
          helperText={formData.errors.title}
        />
        <TextField
          className="form-field"
          name="author"
          label={t('authorLabel')}
          value={formData.author}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.author ? true : false}
          helperText={formData.errors.author}
        />
        <TextField
          className="form-field"
          name="publisher"
          label={t('publisherLabel')}
          value={formData.publisher}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.publisher ? true : false}
          helperText={formData.errors.publisher}
        />
        <TextField
          className="form-field"
          name="publishYear"
          label={t('publishYearLabel')}
          value={formData.publishYear}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.publishYear ? true : false}
          helperText={formData.errors.publishYear}
        />
        <TextField
          className="form-field"
          name="availableCopies"
          label={t('availableCopiesLabel')}
          value={formData.availableCopies}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputProps={{
            classes: {
              focused: 'focused-input',
            },
          }}
          InputLabelProps={{
            classes: {
              focused: 'focused-label',
            },
          }}
          error={formData.errors.availableCopies ? true : false}
          helperText={formData.errors.availableCopies}
        />
        <Button
          className="submit-button"
          variant="contained"
          type="submit"
          style={{
            backgroundColor: isFormValid() ? '#555555' : '#ff6666',
            color: '#fff',
            fontWeight: 'bold',
            textAlign: 'center',
            width: '100%',
            display: 'block',
          }}
          disabled={!isFormValid()} //przycisk dodania książki jest zablokowany (czerwony), póki każde z pól nie jest poprawne
        >
          {t('addBookButtonLabel')}
        </Button>
        <Tooltip title={t('backToHome')}>
          <HomeIcon
            className="back-to-home-icon"
            onClick={() => navigate('/home')}
          />
        </Tooltip>
      </form>
    </Paper>
  );
}
