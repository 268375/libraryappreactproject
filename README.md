# LISTA 6 TECHNOLOGIE SIECIOWE
### NA LABORATORIUM
Zaimplementowałam stronę logowania do aplikacji bibliotecznej (zgodnie z poleceniem skorzystałam z MUI, React Hooks, Yup i Formik).
### W DOMU
Zaimplementowałam również stronę do wyświetlania książek znajdujących się w bibliotece (skorzystałam z różnych komponentów MUI, a strukturę wyświetlania książek dostosowałam do danych, które mam zaimplementowane w backendzie w encji książki). Stronę zaprojektowałam, w taki sposób, by była "user friendly" tzn. użytkownik ma możliwość wyszukania książki np. o interesującym go tytule, czy numerze ISBN, ponadto ma opcję zmiany ilości książek wyświetlanej na jednej stronie i ma możliwość swobodnego przechodzenia między stronami.
# LISTA 7 TECHNOLOGIE SIECIOWE
Zaprojektowałam i zaimplementowałam stronę główną aplikacji bibliotecznej, wykonałam również stronę umożliwiającą wyświetlanie wypożyczeń, zaktualizowałam także stronę do wyświetlania książek. Ponadto zrealizowałam również routing między stworzonymi stronami (po zalogowaniu i naciśnięciu SIGN-IN następuje przejście do strony głównej (homepage), z głównej strony poprzez wybranie odpowiedniej opcji z menu można przejść do strony wyświetlającej książki (list of books) lub strony wyświetlającej wypożyczenia (list of loans), z obu tych stron można powrócić do strony głównej (homepage) poprzez kliknięcie na ikonę domu. Z głównej strony można z kolei się wylogować i powrócić do strony logowania do biblioteki poprzez kliknięcie na ikonę log-out.)
# LISTA 8 TECHNOLOGIE SIECIOWE
Połączyłam strony logowania, listy książek, listy wypożyczeń i listy użytkowników z serwerem Spring oraz stworzyłam i połączyłam z serwerem Spring strony do dodawania książek, użytkowników i wypożyczeń. Obejrzałam również film o lokalizacji w React i stworzyłam 2 wersje każdej ze stron: w języku polskim i angielskim.
# PROJEKT TECHNOLOGIE SIECIOWE - FRONTEND
## OPIS FUNKCJONALNOŚCI ZREALIZOWANYCH NA FRONTENDZIE
### BIBLIOTEKARZ (LIBRARIAN)
• logowanie do systemu bibliotecznego po podaniu loginu i hasła (zaimplementowane
zabezpieczenia związane z podaniem nieprawidłowych danych)

• możliwość wyświetlenia listy wszystkich dostępnych w bibliotece książek (funkcja
zmiany ilości wyświetlanych na stronie pozycji, przechodzenie pomiędzy stronami
oraz wizualne rozróżnienie książek dostępnych i niedostępnych)

• możliwość wyszukania książki według kategorii (autor, tytuł, wydawnictwo, rok wydania,
ISBN)

• możliwość dodania nowej książki po podaniu wszystkich wymaganych danych (w
przypadku podania błędnych lub niepełnych danych zaimplementowałam stosowne
komunikaty)

• możliwość aktualizacji danych dotyczących książki takich jak: autor, tytuł, rok wydania,
wydawnictwo, ISBN, liczba dostępnych kopii po podaniu jej id

• możliwość usunięcia książki po podaniu jej id

• możliwość wyświetlenia listy wypożyczeń będących aktualnie w systemie bibliotecznym
oraz ich wyszukiwania

• możliwość dodania nowego wypożyczenia po podaniu id użytkownika, id książki,
daty wypożyczenia i daty zwrotu

• możliwość dokonania zwrotu wypożyczenia po podaniu id wypożyczenia i daty zwrotu

• możliwość wyświetlenia list wszystkich użytkowników biblioteki oraz ich wyszukiwania

• możliwość dodania nowego użytkownika po podaniu wszystkich wymaganych danych
(w przypadku podania błędnych lub niepełnych danych zaimplementowałam
stosowne komunikaty)

• możliwość aktualizacji danych dotyczących użytkownika takich jak: nazwa, pełna
nazwa użytkownika, email i rola, po podaniu id użytkownika

• możliwość usunięcia użytkownika po podaniu jego id

• możliwość wyświetlenia informacji dotyczących swojego konta (nazwa, pełna nazwa,
email i rola)

• możliwość dokonania edycji informacji dotyczących swojego konta bibliotecznego
(nazwa, pełna nazwa, email i hasło)

• możliwość zmiany języka wyświetlania informacji na stronach (do wyboru jest język
polski i angielski)

• możliwość wylogowania się z systemu bibliotecznego

### CZYTELNIK (READER)
• logowanie do biblioteki online po podaniu loginu i hasła (zaimplementowane zabezpieczenia
związane z podaniem nieprawidłowych danych)

• możliwość wyświetlenia listy wszystkich dostępnych w bibliotece książek (funkcja
zmiany ilości wyświetlanych na stronie pozycji, przechodzenie pomiędzy stronami
oraz wizualne rozróżnienie książek dostępnych i niedostępnych)

• możliwość wyszukania książki według kategorii (autor, tytuł, wydawnictwo, rok wydania,
ISBN)

• możliwość zobaczenia historii swoich wypożyczeń (wypożyczenia zwrócone dla odróżnienia
od wypożyczeń aktualnych mają zaimplementowaną inną kolorystykę kafelków)

• możliwość wyświetlenia informacji dotyczących swojego konta (nazwa, pełna nazwa,
email i rola)

• możliwość dokonania edycji informacji dotyczących swojego konta bibliotecznego
(nazwa, pełna nazwa, email i hasło)

• możliwość zmiany języka wyświetlania informacji na stronach (do wyboru jest język
polski i angielski)

• możliwość wylogowania się z systemu bibliotecznego


